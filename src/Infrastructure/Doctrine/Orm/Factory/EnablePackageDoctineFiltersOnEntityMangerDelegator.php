<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Infrastructure\Doctrine\Orm\Factory;

use Smtm\Base\Infrastructure\Helper\DateTimeHelper;
use Doctrine\ORM\EntityManager;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

class EnablePackageDoctineFiltersOnEntityMangerDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var EntityManager $object */
        $object = $callback();
        $now = DateTimeHelper::format(new \DateTimeImmutable(), DateTimeHelper::FORMAT_SQL_DATETIME);
        $object->getFilters()->enable(
            \Smtm\AuthProvider\Context\AuthCode\Infrastructure\Doctrine\Orm\Query\Filter\ExcludeArchivedFilter::NAME
        );
        $object->getFilters()->enable(
            \Smtm\AuthProvider\Context\AuthCode\Infrastructure\Doctrine\Orm\Query\Filter\AuthCodeFilter::NAME
        )->setParameter(
            'now',
            $now
        );
        $object->getFilters()->enable(
            \Smtm\AuthProvider\Context\Token\Infrastructure\Doctrine\Orm\Query\Filter\ExcludeArchivedFilter::NAME
        );
        $object->getFilters()->enable(
            \Smtm\AuthProvider\Context\Token\Infrastructure\Doctrine\Orm\Query\Filter\TokenFilter::NAME
        )->setParameter(
            'now',
            $now
        );

        return $object;
    }
}
