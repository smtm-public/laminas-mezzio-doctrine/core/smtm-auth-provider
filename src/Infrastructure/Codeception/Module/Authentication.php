<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Infrastructure\Codeception\Module;

use Codeception\Lib\Interfaces\DependsOnModule;
use Codeception\Module;
use Smtm\Auth\Authentication\Application\Service\AuthenticationService;
use Smtm\Auth\Authentication\Application\Service\AuthenticationServicePluginManager;
use Smtm\Auth\Authentication\Application\Service\Cli\CliAuthenticationServiceInterface;
use Smtm\Auth\Context\Client\Domain\Client as AuthClient;
use Smtm\Auth\Context\User\Domain\User as AuthUser;
use Smtm\Auth\Context\UserClient\Domain\UserClient as AuthUserClient;
use Smtm\AuthConsumer\Context\Token\Domain\Token as AuthConsumerToken;
use Smtm\AuthProvider\Authentication\Application\Service\Bearer\BearerAuthenticationService;
use Smtm\AuthProvider\Context\Token\Domain\Token as AuthProviderToken;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Codeception\Module\DataFactory;
use Smtm\Base\Infrastructure\Codeception\Module\Doctrine3;
use Smtm\Base\Infrastructure\Codeception\Module\Mezzio;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Authentication extends Module implements DependsOnModule
{
    protected Mezzio $dependentModule;
    protected Doctrine3 $doctrineModule;
    protected DataFactory $dataFactoryModule;

    public function _initialize()
    {
        parent::_initialize();

        $this->doctrineModule = $this->getModule(Doctrine3::class);
        $this->dataFactoryModule = $this->getModule(DataFactory::class);
    }

    public function _depends(): array
    {
        return [$this->config['depends']];
    }

    public function _inject(Mezzio $dependentModule = null): void
    {
        $this->dependentModule = $dependentModule;
    }

    public function amBearerAuthenticated(array $userCriteria, array $clientCriteria): AuthProviderToken
    {
        $authUser = $this->doctrineModule->grabEntityFromRepository(AuthUser::class, $userCriteria);
        $authClient = $this->doctrineModule->grabEntityFromRepository(AuthClient::class, $clientCriteria);
        $authUserClient = $this->doctrineModule->grabEntityFromRepository(
            AuthUserClient::class,
            [
                'user' => $authUser,
                'client' => $authClient,
            ]
        );
        $authProviderToken = $this->dataFactoryModule->have(
            AuthProviderToken::class,
            [
                'token' => $this->dataFactoryModule->have(
                    AuthConsumerToken::class,
                    [
                        'userClient' => $authUserClient,
                    ]
                ),
            ]
        );
        $this
            ->dependentModule
            ->container
            ->get(ApplicationServicePluginManager::class)
            ->get(AuthenticationService::class)
            ->setAuthenticationService(
                $this
                    ->dependentModule
                    ->container
                    ->get(AuthenticationServicePluginManager::class)
                    ->get(
                        BearerAuthenticationService::class
                    )
                    ->setAuthenticatedToken($authProviderToken)
            );

        return $authProviderToken;
    }

    public function amCliAuthenticated(array $userCriteria, array $clientCriteria): AuthUserClient
    {
        $authUser = $this->doctrineModule->grabEntityFromRepository(AuthUser::class, $userCriteria);
        $authClient = $this->doctrineModule->grabEntityFromRepository(AuthClient::class, $clientCriteria);
        $authUserClient = $this->doctrineModule->grabEntityFromRepository(
            AuthUserClient::class,
            [
                'user' => $authUser,
                'client' => $authClient,
            ]
        );
        $this
            ->dependentModule
            ->container
            ->get(ApplicationServicePluginManager::class)
            ->get(AuthenticationService::class)
            ->setAuthenticationService(
                $this
                    ->dependentModule
                    ->container
                    ->get(AuthenticationServicePluginManager::class)
                    ->get(
                        CliAuthenticationServiceInterface::class
                    )
                    ->setAuthenticatedUserClient($authUserClient)
            );

        return $authUserClient;
    }
}
