<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\AuthCode\Application\Hydrator;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthCodeHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'userClient' => 'You must specify a UserClient for the AuthCode.',
        'code' => 'You must specify a code for the AuthCode.',
        'redirectUri' => 'You must specify a redirectUri for the AuthCode.',
        'scope' => 'You must specify a scope for the AuthCode.',
        'expires' => 'You must specify an expiration DateTime for the AuthCode.',
    ];
}
