<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\AuthCode\Application\Service;

use DateTimeImmutable;
use Smtm\Auth\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Auth\Context\Client\Application\Service\ClientService;
use Smtm\Auth\Context\Client\Application\Service\ClientServiceInterface;
use Smtm\Auth\Context\Client\Application\Service\Exception\InvalidClientException;
use Smtm\Auth\Context\Client\Domain\Client;
use Smtm\Auth\Context\Client\Domain\ClientInterface;
use Smtm\Auth\Context\User\Application\Service\UserService;
use Smtm\Auth\Context\User\Application\Service\UserServiceInterface;
use Smtm\Auth\Context\User\Context\AuthFailure\Application\Service\AuthFailureService;
use Smtm\Auth\Context\User\Domain\UserInterface;
use Smtm\Auth\Context\UserClient\Application\Service\UserClientService;
use Smtm\AuthProvider\Context\AuthCode\Application\Hydrator\AuthCodeHydrator;
use Smtm\AuthProvider\Context\AuthCode\Application\Service\Exception\InvalidLoginCredentialsException;
use Smtm\AuthProvider\Context\AuthCode\Application\Service\Exception\InvalidLoginException;
use Smtm\AuthProvider\Context\AuthCode\Application\Service\Exception\PasswordValidationException;
use Smtm\AuthProvider\Context\AuthCode\Application\Service\Exception\UserLockoutException;
use Smtm\AuthProvider\Context\AuthCode\Domain\AuthCode;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceTrait;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityNotFoundException;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Domain\EntityInterface;
use Smtm\Base\Infrastructure\Laminas\Validator\PasswordValidator;
use Smtm\Base\Infrastructure\Laminas\Validator\ValidatorPluginManagerAwareTrait;
use Smtm\Base\Infrastructure\Service\OAuth2Service;
use Laminas\Validator\ValidatorPluginManagerAwareInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthCodeService extends AbstractDbService implements
    ConfigAwareInterface,
    UuidAwareEntityDbServiceInterface,
    ValidatorPluginManagerAwareInterface
{

    use ConfigAwareTrait, UuidAwareEntityDbServiceTrait, ValidatorPluginManagerAwareTrait;

    protected ?string $domainObjectName = AuthCode::class;
    protected ?string $hydratorName = AuthCodeHydrator::class;

    public function createFunc(
        array $data,
        ?EntityInterface $entity = null,
        array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): EntityInterface {
        /** @var ClientService $clientService */
        $clientService = $this->applicationServicePluginManager->get(ClientServiceInterface::class);
        $client = null;
        $user = null;
        $userClient = null;

        try {
            /** @var ClientInterface $client */
            $client = $clientService->getOneBy(['clientId' => $data['clientId']]);
        } catch (EntityNotFoundException $e) {
            throw new InvalidClientException(previous: $e);
        }

        if (!$client->hasRedirectUriWithRedirectUri($data['redirectUri'])) {
            // TODO: MD - Throw error
        }

        /** @var UserService $userService */
        $userService = $this->applicationServicePluginManager->get(UserServiceInterface::class);
        /** @var AuthFailureService $authFailureService */
        $authFailureService = $this->applicationServicePluginManager->get(AuthFailureService::class);
        /** @var UserClientService $userClientService */
        $userClientService = $this->applicationServicePluginManager->get(UserClientService::class);

        try {
            /**
             * ATTENTION!!!
             * MAKE SURE THE USER ID DATA KEY DOES NOT LEAK FROM OUTSIDE ACCIDENTALLY
             * IT IS ONLY SUPPOSED TO COME FROM AN ACTIVE SESSION
             */
            if (isset($data['userId']))  {
                /** @var UserInterface $user */
                $user = $userService->getById($data['userId']);
            } elseif (isset($data['usernameOrEmail']) && isset($data['password'])) {
                $user = $userService->getByClientAndUsernameOrEmail($client, $data['usernameOrEmail']);

                if (!$authFailureService->canRequestAuthentication($user)) {
                    throw new UserLockoutException();
                }

                /** @var UserInterface $user */
                $user = $userService->getByClientAndUsernameOrEmailAndPassword(
                    $client,
                    $data['usernameOrEmail'],
                    $data['password']
                );

                /** @var PasswordValidator $passwordValidator */
                $passwordValidator = $this->validatorPluginManager->get('validator-password-auth');

                // Validate if user password meet the security requirements
                if (!$passwordValidator->isValid($data['password'])) {
                    throw (new PasswordValidationException(implode(' | ', $passwordValidator->getMessages())))
                        ->setRedirectUri(
                            $userService->getResetPasswordUrl(
                                $client,
                                $user,
                                'weak-pass'
                            )
                        );
                }
            }

            $userClient = $userClientService->getOneBy(['user' => $user, 'client' => $client]);
        } catch (UserLockoutException|InvalidLoginException $e) {
            throw $e->setClient($client);
        } catch (EntityNotFoundException $e) {
            throw (new InvalidLoginException(previous: $e))->setClient($client);
        } catch (InvalidLoginCredentialsException $e) {
            $authFailureService->createFunc([
                'user' => $user,
                'created' => new \DateTimeImmutable()
            ]);

            throw $e->setClient($client);
        }

        $authFailureService->clearFailures($user);

        $authCodes = $this->getAll([
            'userClient' => $userClient,
            'revoked' => false,
        ]);

        /** @var AuthCode $authCode */
        foreach ($authCodes as $authCode) {
            parent::updateFunc($authCode, ['revoked' => true], $options);
        }

        $authCode = $this->prepareDomainObject();
        $entityData = [];
        $entityData['userClient'] = $userClient;
        $entityData['redirectUri'] = $data['redirectUri'];
        $entityData['scope'] = $data['scope'];
        $entityData['state'] = $data['state'] ?? null;
        $entityData['expires'] = (new DateTimeImmutable())->modify(
            '+' . $this->config['authCodeExpirationSeconds'] . ' seconds'
        );
        $entityData['codeChallenge'] = $data['codeChallenge'];
        $entityData['codeChallengeMethod'] = $data['codeChallengeMethod'];
        /** @var OAuth2Service $oAuth2Service */
        $oAuth2Service = $this->infrastructureServicePluginManager->get(OAuth2Service::class);
        $entityData['code'] = $oAuth2Service->generateAuthCode(
            $entityData + ['uuid' => $authCode->getUuid()]
        );

        return parent::createFunc($entityData, $authCode, $options);
    }

    public function revokeAllForUserId(
        int $userId,
        array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): void {
        $this->transactional(
            function () use ($userId, $options) {
                $authCodes = $this->getAll([
                    'userClient.user.id' => $userId,
                    'revoked' => false,
                ]);

                /** @var AuthCode $authCode */
                foreach ($authCodes as $authCode) {
                    parent::updateFunc($authCode, ['revoked' => true], $options);
                }
            }
        );
    }

    public function getByClientAndCode(Client $client, string $code): AuthCode
    {
        try {
            /** @var AuthCode $authCode */
            $authCode = $this->getOneBy(['code' => $code]);
        } catch (EntityNotFoundException $e) {
            throw new EntityNotFoundException(
                EntityNotFoundException::formatMessageForEntity('AuthCode'),
                0,
                $e->getPrevious()
            );
        }

        if (!$authCode->verifyClient($client)) {
            throw new EntityNotFoundException(
                EntityNotFoundException::formatMessageForEntity('AuthCode'),
                0
            );
        }

        return $authCode;
    }
}
