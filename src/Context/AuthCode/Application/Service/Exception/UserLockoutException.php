<?php

namespace Smtm\AuthProvider\Context\AuthCode\Application\Service\Exception;

use Smtm\Auth\Context\Client\Domain\ClientInterface;
use Smtm\Base\Http\Exception\RuntimeException;

class UserLockoutException extends RuntimeException
{
    public const MESSAGE = 'ACCOUNT_LOCKED';
    public const CODE = 0;

    protected $message = self::MESSAGE;
    protected $code = self::CODE;

    protected ClientInterface $client;

    public function __construct(string $message = null, int $code = null, ?\Throwable $previous = null)
    {
        parent::__construct($message ?? static::MESSAGE, $code ?? static::CODE, $previous);
    }

    public function getClient(): ClientInterface
    {
        return $this->client;
    }

    public function setClient(ClientInterface $client): static
    {
        $this->client = $client;

        return $this;
    }
}
