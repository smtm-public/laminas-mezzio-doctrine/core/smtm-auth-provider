<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\AuthCode\Application\Service\Exception;

use Smtm\Base\Http\Middleware\Mezzio\ProblemDetails\Exception\CommonProblemDetailsExceptionTrait;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Mezzio\ProblemDetails\Exception\ProblemDetailsExceptionInterface;
use RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class PasswordValidationException extends RuntimeException implements ProblemDetailsExceptionInterface
{

    use CommonProblemDetailsExceptionTrait;

    protected string $redirectUri;

    public function getRedirectUri(): string
    {
        return $this->redirectUri;
    }

    public function setRedirectUri(string $redirectUri): static
    {
        $this->redirectUri = $redirectUri;

        return $this;
    }

    public function getClass(): string
    {
        return static::class;
    }

    public function getStatus(): int
    {
        return HttpHelper::STATUS_CODE_UNAUTHORIZED;
    }

    public function getDetail(): string
    {
        return 'Weak password.';
    }

    public function getTitle(): string
    {
        return 'Authentication Exception';
    }

    public function getType(): string
    {
        return 'example.com';
    }
}
