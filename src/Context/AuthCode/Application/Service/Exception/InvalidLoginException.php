<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\AuthCode\Application\Service\Exception;

use Smtm\Auth\Context\Client\Domain\ClientInterface;
use RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InvalidLoginException extends RuntimeException
{
    public const MESSAGE = 'INVALID_LOGIN';
    public const CODE = 0x100001;

    protected $message = self::MESSAGE;
    protected $code = self::CODE;

    protected ClientInterface $client;

    public function getClient(): ClientInterface
    {
        return $this->client;
    }

    public function setClient(ClientInterface $client): static
    {
        $this->client = $client;

        return $this;
    }
}
