<?php

namespace Smtm\AuthProvider\Context\AuthCode\Application\Service\Exception;

use Smtm\Base\Http\Exception\RuntimeException;

class InvalidResponseTypeException extends RuntimeException
{
    public const MESSAGE = 'INVALID_RESPONSE_TYPE';
    public const CODE = 0;

    protected $message = self::MESSAGE;
    protected $code = self::CODE;

    public function __construct(string $message = null, int $code = null, ?\Throwable $previous = null)
    {
        parent::__construct($message ?? static::MESSAGE, $code ?? static::CODE, $previous);
    }
}
