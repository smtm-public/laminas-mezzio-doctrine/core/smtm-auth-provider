<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\AuthCode\Application\Service\Exception;

use Smtm\Auth\Context\Client\Domain\ClientInterface;
use Smtm\Base\Http\Middleware\Mezzio\ProblemDetails\Exception\CommonProblemDetailsExceptionTrait;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Mezzio\ProblemDetails\Exception\ProblemDetailsExceptionInterface;
use RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InvalidLoginCredentialsException extends RuntimeException implements ProblemDetailsExceptionInterface
{

    use CommonProblemDetailsExceptionTrait;

    protected ClientInterface $client;

    public function getClient(): ClientInterface
    {
        return $this->client;
    }

    public function setClient(ClientInterface $client): static
    {
        $this->client = $client;

        return $this;
    }

    public function getClass(): string
    {
        return static::class;
    }

    public function getStatus(): int
    {
        return HttpHelper::STATUS_CODE_UNAUTHORIZED;
    }

    public function getDetail(): string
    {
        return 'Invalid login credentials.';
    }

    public function getTitle(): string
    {
        return 'Authentication Exception';
    }

    public function getType(): string
    {
        return 'example.com';
    }
}
