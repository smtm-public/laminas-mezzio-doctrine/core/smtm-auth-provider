<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\AuthCode\Domain;

use DateTimeImmutable;
use Smtm\Auth\Context\Client\Domain\ClientInterface;
use Smtm\Auth\Context\UserClient\Domain\UserClientInterface;
use Smtm\Base\Domain\AbstractUuidAwareEntity;
use Smtm\Base\Domain\CreatedDateTimeImmutableAwareEntityInterface;
use Smtm\Base\Domain\CreatedDateTimeImmutableAwareEntityTrait;
use Smtm\Base\Domain\MarkedForUpdateInterface;
use Smtm\Base\Domain\MarkedForUpdateTrait;
use Smtm\Base\Domain\ModifiedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\ModifiedDateTimeAwareEntityTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthCode extends AbstractUuidAwareEntity implements
    CreatedDateTimeImmutableAwareEntityInterface,
    ModifiedDateTimeAwareEntityInterface,
    MarkedForUpdateInterface
{

    use CreatedDateTimeImmutableAwareEntityTrait,
        ModifiedDateTimeAwareEntityTrait,
        MarkedForUpdateTrait;

    protected UserClientInterface $userClient;
    protected string $code;
    protected string $redirectUri;
    protected string $scope;
    protected DateTimeImmutable $expires;
    protected ?string $codeChallenge = null;
    protected ?string $codeChallengeMethod = null;
    protected ?string $state = null;
    protected bool $used = false;
    protected bool $revoked = false;

    public function getUserClient(): UserClientInterface
    {
        return $this->userClient;
    }

    public function setUserClient(UserClientInterface $userClient): static
    {
        return $this->__setProperty('userClient', $userClient);
    }

    public function verifyClient(ClientInterface $client): bool
    {
        return $this->getUserClient()->getUser()->hasClient($client);
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): static
    {
        return $this->__setProperty('code', $code);
    }

    public function getRedirectUri(): string
    {
        return $this->redirectUri;
    }

    public function setRedirectUri(string $redirectUri): static
    {
        return $this->__setProperty('redirectUri', $redirectUri);
    }

    public function getScope(): string
    {
        return $this->scope;
    }

    public function setScope(string $scope): static
    {
        return $this->__setProperty('scope', $scope);
    }

    public function getExpires(): DateTimeImmutable
    {
        return $this->expires;
    }

    public function setExpires(DateTimeImmutable $expires): static
    {
        return $this->__setProperty('expires', $expires);
    }

    public function isNotExpired(): bool
    {
        return $this->expires >= new DateTimeImmutable();
    }

    public function getCodeChallenge(): ?string
    {
        return $this->codeChallenge;
    }

    public function setCodeChallenge(?string $codeChallenge): static
    {
        return $this->__setProperty('codeChallenge', $codeChallenge);
    }

    public function getCodeChallengeMethod(): ?string
    {
        return $this->codeChallengeMethod;
    }

    public function setCodeChallengeMethod(?string $codeChallengeMethod): static
    {
        return $this->__setProperty('codeChallengeMethod', $codeChallengeMethod);
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): static
    {
        return $this->__setProperty('state', $state);
    }

    public function getUsed(): bool
    {
        return $this->used;
    }

    public function setUsed(bool $used): static
    {
        return $this->__setProperty('used', $used);
    }

    public function getRevoked(): bool
    {
        return $this->revoked;
    }

    public function setRevoked(bool $revoked): static
    {
        return $this->__setProperty('revoked', $revoked);
    }
}
