<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\AuthCode\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Repository\RepositoryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface AuthCodeRepositoryInterface extends RepositoryInterface
{

}
