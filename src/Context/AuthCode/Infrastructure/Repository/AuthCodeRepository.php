<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\AuthCode\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthCodeRepository extends AbstractRepository implements AuthCodeRepositoryInterface
{

}
