<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\AuthCode\Infrastructure\Doctrine\Orm\Query\Filter;

use Smtm\AuthProvider\Context\AuthCode\Domain\AuthCode;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ExcludeArchivedFilter extends SQLFilter
{
    public const NAME = 'excludeSmtmAuthProviderAuthCode';

    /**
     * @inheritDoc
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, string $targetTableAlias): string
    {
        if ($targetEntity->getReflectionClass()->name === AuthCode::class) {
            return sprintf(
                <<< EOT
                EXISTS(
                    SELECT
                        id
                    FROM
                        auth_user_client
                    WHERE
                        auth_user_client.id=%s.auth_user_client_id
                        AND auth_user_client.not_archived = 1
                )
                EOT,
                $targetTableAlias
            );
        }

        return '';
    }
}
