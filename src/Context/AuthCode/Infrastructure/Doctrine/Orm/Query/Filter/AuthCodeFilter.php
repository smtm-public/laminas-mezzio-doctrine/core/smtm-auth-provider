<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\AuthCode\Infrastructure\Doctrine\Orm\Query\Filter;

use Smtm\AuthProvider\Context\AuthCode\Domain\AuthCode;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthCodeFilter extends SQLFilter
{
    public const NAME = 'smtmAuthProviderAuthCodeFilter';

    /**
     * @inheritDoc
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, string $targetTableAlias): string
    {
        if ($targetEntity->getReflectionClass()->name === AuthCode::class) {
            return sprintf(
                '%s.used = 0 and %s.revoked = 0 and %s.expires > ' . $this->getParameter('now'),
                $targetTableAlias,
                $targetTableAlias,
                $targetTableAlias,
            );
        }

        return '';
    }
}
