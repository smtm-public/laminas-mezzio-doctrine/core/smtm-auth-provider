<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\AuthCode\Http\Handler;

use Smtm\Auth\Context\Client\Application\Service\Exception\InvalidClientException;
use Smtm\AuthProvider\Context\AuthCode\Application\Service\AuthCodeService;
use Smtm\AuthProvider\Context\AuthCode\Application\Service\Exception\InvalidLoginCredentialsException;
use Smtm\AuthProvider\Context\AuthCode\Application\Service\Exception\InvalidLoginException;
use Smtm\AuthProvider\Context\AuthCode\Application\Service\Exception\InvalidResponseTypeException;
use Smtm\AuthProvider\Context\AuthCode\Application\Service\Exception\PasswordValidationException;
use Smtm\AuthProvider\Context\AuthCode\Application\Service\Exception\UserLockoutException;
use Smtm\AuthProvider\Application\Exception\UserNotFoundException;
use Smtm\AuthProvider\Context\AuthCode\Domain\AuthCode;
use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Http\Handler\DbServiceEntity\AbstractCreateHandler;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Smtm\Base\Infrastructure\Helper\OAuth2Helper;
use Laminas\Diactoros\Response\RedirectResponse;
use Mezzio\Session\SessionInterface;
use Mezzio\Session\SessionMiddleware;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthCodeHandler extends AbstractCreateHandler implements ConfigAwareInterface
{

    use ConfigAwareTrait;

    public ?string $applicationServiceName = AuthCodeService::class;
    /** @var AuthCodeService $applicationService */
    protected ?AbstractApplicationService $applicationService;

    /**
     * @SWG\Post(path="/experimental/sso/sso/token",
     *   tags={"Token"},
     *   summary="Creates an SSO Token from auth code and return it back for further authentication",
     *   description="Creates access token by given authCode OR refreshToken",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *   @SWG\Response(
     *     response=201,
     *     description="Successfully saved data",
     *     @SWG\Schema(
     *       @SWG\Property(
     *              property="accessToken",
     *              type="string"
     *       ),
     *       @SWG\Property(
     *              property="refreshToken",
     *              type="string"
     *       ),
     *     )
     *   ),
     *   @SWG\Response(response=409, description="Invalid data"),
     *   security={{"Bearer":{}}}
     * )
     */
    public function handle(ServerRequestInterface $request): RedirectResponse
    {
        /** @var SessionInterface $session */
        $session = $request->getAttribute(SessionMiddleware::SESSION_ATTRIBUTE);
        $parsedBody = $request->getParsedBody();

        try {
            if ($parsedBody['response_type'] === OAuth2Helper::AUTHORIZATION_REQUEST_RESPONSE_TYPE_CODE) {
                /** @var AuthCode $authCode */
                $authCode = $this->applicationService->create(
                    [
                        'clientId' => $parsedBody['client_id'],
                        'redirectUri' => $parsedBody['redirect_uri'],
                        'responseType' => $parsedBody['response_type'],
                        'scope' => $parsedBody['scope'],
                        'state' => $parsedBody['state'] ?? null,
                        'usernameOrEmail' => $parsedBody['username'],
                        'password' => $parsedBody['password'],
                        'codeChallenge' => $parsedBody['code_challenge'] ?? null,
                        'codeChallengeMethod' => $parsedBody['code_challenge_method'] ?? null,
                    ]
                );

                $session->regenerate();
                $session->set('userId', $authCode->getUserClient()->getUser()->getId());

                return new RedirectResponse(
                    HttpHelper::urlAddQueryParams(
                        $authCode->getRedirectUri(),
                        [
                            'code' => $authCode->getCode(),
                            'state' => $authCode->getState(),
                        ]
                    )
                );
            }
        } catch (InvalidClientException $e) {
            if ($this->config['debug']['authCodeCreateThrowOnErrorNoRedirect']) {
                throw $e;
            } else {
                return new RedirectResponse(
                    HttpHelper::urlAddQueryParams(
                        $parsedBody['redirect_uri'],
                        [
                            'errorCode' => InvalidClientException::CODE,
                            'errorMessage' => InvalidClientException::MESSAGE,
                        ]
                    )
                );
            }
        } catch (InvalidLoginException | UserNotFoundException | InvalidLoginCredentialsException $e) {
            if ($this->config['debug']['authCodeCreateThrowOnErrorNoRedirect']) {
                throw $e;
            } else {
                return new RedirectResponse(
                    HttpHelper::urlAddQueryParams(
                        $this->config['clientOptions'][$e->getClient()->getClientId()]['returnUri']['error'] ?? $parsedBody['redirect_uri'],
                        [
                            'errorCode' => InvalidLoginException::CODE,
                            'errorMessage' => InvalidLoginException::MESSAGE,
                        ]
                    )
                );
            }
        } catch (UserLockoutException $e) {
            if ($this->config['debug']['authCodeCreateThrowOnErrorNoRedirect']) {
                throw $e;
            } else {
                return new RedirectResponse(
                    HttpHelper::urlAddQueryParams(
                        $this->config['clientOptions'][$e->getClient()->getClientId()]['returnUri']['error'] ?? $parsedBody['redirect_uri'],
                        [
                            'errorCode' => UserLockoutException::CODE,
                            'errorMessage' => UserLockoutException::MESSAGE,
                        ]
                    )
                );
            }
        } catch (PasswordValidationException $e) {
            if ($this->config['debug']['authCodeCreateThrowOnErrorNoRedirect']) {
                throw $e;
            } else {
                return new RedirectResponse($e->getRedirectUri());
            }
        }

        if ($this->config['debug']['authCodeCreateThrowOnErrorNoRedirect']) {
            throw new InvalidResponseTypeException('Invalid response type');
        }

        return new RedirectResponse(
            HttpHelper::urlAddQueryParams(
                $parsedBody['redirect_uri'],
                [
                    'errorCode' => InvalidResponseTypeException::CODE,
                    'errorMessage' => InvalidResponseTypeException::MESSAGE,
                ]
            )
        );
    }
}
