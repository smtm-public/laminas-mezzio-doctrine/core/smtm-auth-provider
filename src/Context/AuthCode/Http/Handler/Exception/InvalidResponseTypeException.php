<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\AuthCode\Http\Handler\Exception;

use Smtm\Base\Http\Exception\BadRequestException;
use Smtm\Base\Infrastructure\Helper\HttpHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InvalidResponseTypeException extends BadRequestException
{
    public const MESSAGE = 'Invalid response type specified';
    public const CODE = 0x100003;
    public const STATUS = HttpHelper::STATUS_CODE_BAD_REQUEST;
    public const TYPE = 'https://example.com/problems/bad-request';

    protected $message = self::MESSAGE;
    protected $code = self::CODE;
    protected int $status = self::STATUS;
    protected string $title = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_BAD_REQUEST];
    protected string $detail = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_BAD_REQUEST];
    protected string $type = self::TYPE;
}
