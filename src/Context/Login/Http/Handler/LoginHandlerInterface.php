<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\Login\Http\Handler;

use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\RedirectResponse;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface LoginHandlerInterface
{
    public function handle(ServerRequestInterface $request): HtmlResponse | RedirectResponse;

    public function getLayoutTemplateName(): string;
    public function getPageTemplateName(): string;
}
