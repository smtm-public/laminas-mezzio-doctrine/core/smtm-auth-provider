<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\Login\Http\Handler;

use Smtm\AuthProvider\Context\AuthCode\Application\Service\AuthCodeService;
use Smtm\AuthProvider\Context\AuthCode\Application\Service\Exception\InvalidLoginException;
use Smtm\AuthProvider\Context\AuthCode\Application\Service\Exception\UserLockoutException;
use Smtm\AuthProvider\Context\AuthCode\Domain\AuthCode;
use Smtm\AuthProvider\Context\AuthCode\Http\Handler\Exception\InvalidResponseTypeException;
use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Http\Handler\AbstractHandler;
use Smtm\Base\Http\Middleware\ServerParamsAndRequestIdMiddleware;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Smtm\Base\Infrastructure\Helper\OAuth2Helper;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\RedirectResponse;
use Laminas\View\Model\ViewModel;
use Mezzio\Session\SessionInterface;
use Mezzio\Session\SessionMiddleware;
use Mezzio\Template\TemplateRendererInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class LoginHandler extends AbstractHandler implements RequestHandlerInterface, ConfigAwareInterface, LoginHandlerInterface
{

    use ConfigAwareTrait;

    public function __construct(
        protected ApplicationServicePluginManager $applicationServicePluginManager,
        protected InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected ExtractorPluginManager $extractorPluginManager,
        protected array $config,
        protected ?TemplateRendererInterface $template = null
    ) {

    }

    public function handle(ServerRequestInterface $request): HtmlResponse | RedirectResponse
    {
        /** @var SessionInterface $session */
        $session = $request->getAttribute(SessionMiddleware::SESSION_ATTRIBUTE);
        $queryParams = $request->getQueryParams();
        $parsedQueryParams = static::parseQueryParams(static::getMergedFilteredQueryParams($request));

        if ($session->get('userId') === null) {
            return new HtmlResponse(
                $this->template->render(
                    $this->getPageTemplateName(),
                    [
                        'clientId' => $queryParams['client_id'],
                        'redirectUri' => $queryParams['redirect_uri'],
                        'responseType' => $queryParams['response_type'],
                        'scope' => $queryParams['scope'],
                        'codeChallenge' => $queryParams['code_challenge'] ?? null,
                        'codeChallengeMethod' => $queryParams['code_challenge_method'] ?? null,
                        'state' => $queryParams['state'] ?? null,
                        'layout' => (new ViewModel(['title' => 'Login']))->setTemplate(
                            $this->getLayoutTemplateName()
                        ),
                    ]
                )
            );
        }

        /** @var AuthCodeService $authCodeService */
        $authCodeService = $this->applicationServicePluginManager->get(AuthCodeService::class);

        if ($queryParams['response_type'] === OAuth2Helper::AUTHORIZATION_REQUEST_RESPONSE_TYPE_CODE) {
            try {
                /** @var AuthCode $authCode */
                $authCode = $authCodeService->create(
                    [
                        'userId' => $session->get('userId'),
                        'clientId' => $queryParams['client_id'],
                        'redirectUri' => $queryParams['redirect_uri'],
                        'responseType' => $queryParams['response_type'],
                        'scope' => $queryParams['scope'],
                        'state' => $queryParams['state'] ?? null,
                        'codeChallenge' => $queryParams['code_challenge'] ?? null,
                        'codeChallengeMethod' => $queryParams['code_challenge_method'] ?? null,
                    ],
                    null,
                    [
                        ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS =>
                            $request->getAttribute(
                                ServerParamsAndRequestIdMiddleware::REQUEST_ATTRIBUTE_NAME_REMOTE_ADDRESS_HEADER_FORWARDED
                            ) ?? $request->getAttribute(
                                ServerParamsAndRequestIdMiddleware::REQUEST_ATTRIBUTE_NAME_REMOTE_ADDRESS
                            ),
                        ApplicationServiceInterface::OPTION_KEY_PARAMS => $parsedQueryParams,
                        ApplicationServiceInterface::OPTION_KEY_METHOD => $request->getMethod(),
                    ]
                );
            } catch (InvalidLoginException $e) {
                if ($this->config['debug']['authCodeCreateThrowOnErrorNoRedirect']) {
                    throw $e;
                } else {
                    return new RedirectResponse(
                        HttpHelper::urlAddQueryParams(
                            $this->config['clientOptions'][$queryParams['client_id']]['returnUri']['error'],
                            [
                                'errorCode' => InvalidLoginException::CODE,
                                'errorMessage' => InvalidLoginException::MESSAGE,
                            ]
                        )
                    );
                }
            } catch (UserLockoutException $e) {
                if ($this->config['debug']['authCodeCreateThrowOnErrorNoRedirect']) {
                    throw $e;
                } else {
                    return new RedirectResponse(
                        HttpHelper::urlAddQueryParams(
                            $this->config['clientOptions'][$queryParams['client_id']]['returnUri']['error'],
                            [
                                'errorCode' => UserLockoutException::CODE,
                                'errorMessage' => UserLockoutException::MESSAGE,
                            ]
                        )
                    );
                }
            }

            $session->regenerate();

            return new RedirectResponse(
                HttpHelper::urlAddQueryParams(
                    $authCode->getRedirectUri(),
                    [
                        'code' => $authCode->getCode(),
                        'state' => $authCode->getState(),
                    ]
                )
            );
        }

        return new RedirectResponse(
            HttpHelper::urlAddQueryParams(
                $this->config['clientOptions'][$queryParams['client_id']]['returnUri']['error'],
                [
                    'errorCode' => InvalidResponseTypeException::CODE,
                    'errorMessage' => InvalidResponseTypeException::MESSAGE,
                ]
            )
        );
    }

    public function getLayoutTemplateName(): string
    {
        return 'layout::bootstrap4-default';
    }

    public function getPageTemplateName(): string
    {
        return 'smtm-auth-page::login/content';
    }
}
