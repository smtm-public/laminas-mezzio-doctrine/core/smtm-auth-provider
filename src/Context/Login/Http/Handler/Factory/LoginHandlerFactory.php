<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\Login\Http\Handler\Factory;

use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Mezzio\Template\TemplateRendererInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class LoginHandlerFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new $requestedName(
            $container->get(ApplicationServicePluginManager::class),
            $container->get(InfrastructureServicePluginManager::class),
            $container->get(ExtractorPluginManager::class),
            $container->get('config')['base']['http']['handler'],
            $container->get(TemplateRendererInterface::class)
        );
    }
}
