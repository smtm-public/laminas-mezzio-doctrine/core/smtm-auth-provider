<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\Logout\Http\InputFilter;

use Smtm\Base\Http\InputFilter\AbstractRequestValidatingInputFilterCollection;
use Smtm\Base\Infrastructure\Helper\OAuth2Helper;
use Smtm\Base\Infrastructure\Laminas\InputFilter\InputFilterSpecificationInterface;
use Smtm\Base\Infrastructure\Laminas\Validator\InArray;
use Smtm\Base\Infrastructure\Laminas\Validator\IsString;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class LogoutHandlerRequestValidatingInputFilterCollection extends AbstractRequestValidatingInputFilterCollection
{
    protected array $queryParamsInputFilterSpecification = [
        InputFilterSpecificationInterface::INPUT_COLLECTION => [
            [
                'name' => 'client_id',
                'required' => true,
                'validators' => [
                    [
                        'name' => IsString::class,
                        'options' => [
                            'minLength' => 32,
                            'maxLength' => 32,
                            'hexadecimal' => IsString::OPTION_HEXADECIMAL_CASE_LOWER,
                        ],
                    ],
                ],
            ],
            [
                'name' => 'redirect_uri',
                'required' => true,
                'validators' => [
                    [
                        'name' => IsString::class,
                        'options' => [
                            'minLength' => 8,
                            'maxLength' => 255
                        ],
                    ],
                ],
            ],
            [
                'name' => 'response_type',
                'required' => true,
                'validators' => [
                    [
                        'name' => IsString::class,
                    ],
                    [
                        'name' => InArray::class,
                        'options' => [
                            'haystack' => OAuth2Helper::AUTHORIZATION_REQUEST_RESPONSE_TYPES,
                        ],
                    ],
                ],
            ],
            [
                'name' => 'scope',
                'required' => true,
                'validators' => [
                    [
                        'name' => IsString::class,
                        'options' => [
                            'minLength' => 1,
                        ],
                    ],
                ],
            ],
            [
                'name' => 'code_challenge',
                'required' => false,
                'validators' => [
                    [
                        'name' => IsString::class,
                        'options' => [
                            'minLength' => 43,
                            'maxLength' => 128,
                        ],
                    ],
                ],
            ],
            [
                'name' => 'code_challenge_method',
                'required' => false,
                'validators' => [
                    [
                        'name' => IsString::class,
                    ],
                    [
                        'name' => InArray::class,
                        'options' => [
                            'haystack' => OAuth2Helper::PKCE_CODE_CHALLENGE_METHODS,
                        ],
                    ],
                ],
            ],
        ],
    ];

    public function validate(): array
    {
        if ($this->queryParamsInputFilter->getValue('code_challenge') !== null) {
            $this->queryParamsInputFilter->get('code_challenge_method')->setRequired(true);
        }

        return parent::validate();
    }
}
