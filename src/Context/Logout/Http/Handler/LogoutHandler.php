<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\Logout\Http\Handler;

use Smtm\AuthProvider\Context\AuthCode\Application\Service\AuthCodeService;
use Smtm\AuthProvider\Context\Token\Application\Service\TokenService;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Http\Middleware\ServerParamsAndRequestIdMiddleware;
use Smtm\Base\Infrastructure\Helper\ArrayHelper;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Smtm\Base\Infrastructure\Helper\LetterCaseHelper;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use http\Url;
use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\RedirectResponse;
use Mezzio\Router\RouteResult;
use Mezzio\Session\SessionInterface;
use Mezzio\Session\SessionMiddleware;
use Mezzio\Template\TemplateRendererInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class LogoutHandler implements RequestHandlerInterface
{
    public function __construct(
        protected ApplicationServicePluginManager $applicationServicePluginManager,
        protected InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected ?TemplateRendererInterface $template = null
    ) {

    }

    public function handle(ServerRequestInterface $request): HtmlResponse | RedirectResponse
    {
        /** @var SessionInterface $session */
        $session = $request->getAttribute(SessionMiddleware::SESSION_ATTRIBUTE);
        $userId = $session->get('userId');
        $session->unset('userId');

        if ($userId !== null) {
            /** @var AuthCodeService $authCodeService */
            $authCodeService = $this->applicationServicePluginManager->get(AuthCodeService::class);
            $authCodeService->revokeAllForUserId($userId);

            /** @var TokenService $tokenService */
            $tokenService = $this->applicationServicePluginManager->get(TokenService::class);
            $tokenService->revokeAllForUserId($userId);
        }

        $queryParams = $request->getQueryParams();

        $paramsAuth = [
            'clientId' => $queryParams['client_id'],
            'redirectUri' => $queryParams['redirect_uri'],
            'responseType' => $queryParams['response_type'],
            'scope' => $queryParams['scope'],
            'codeChallenge' => $queryParams['code_challenge'] ?? null,
            'codeChallengeMethod' => $queryParams['code_challenge_method'] ?? null,
            'state' => $queryParams['state'] ?? null,
        ];

        $redirectUriPath = '/auth-provider/login';
        $headerForwardedParsed = $request->getAttribute(
            ServerParamsAndRequestIdMiddleware::REQUEST_ATTRIBUTE_NAME_HEADER_FORWARDED_PARSED
        );

        if ($headerForwardedParsed !== null && $headerForwardedParsed['path'] !== null) {
            /** @var RouteResult $routeResult */
            $routeResult = $request->getAttribute(RouteResult::class);
            $initialPath = $headerForwardedParsed['path'][array_key_first($headerForwardedParsed['path'])];
            $pathPrefix = substr(
                $initialPath,
                0,
                strpos($initialPath, $routeResult->getMatchedRoute()->getPath())
            );
            $redirectUriPath = $pathPrefix . $redirectUriPath;
        }

        return new RedirectResponse(
            HttpHelper::urlAddQueryParams(
                (string) new Url(parse_url($redirectUriPath)),
                ArrayHelper::convertKeyLetterCase(
                    $paramsAuth,
                    [LetterCaseHelper::LETTER_CASE_SNAKE, LetterCaseHelper::LETTER_CASE_LOWER]
                )
            )
        );
    }
}
