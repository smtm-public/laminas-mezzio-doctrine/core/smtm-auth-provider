<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\Test\Http\Handler;

use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Infrastructure\Helper\ArrayHelper;
use Smtm\Base\Infrastructure\Helper\LetterCaseHelper;
use Smtm\Base\Infrastructure\Helper\OAuth2Helper;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\View\Model\ViewModel;
use Mezzio\Session\SessionInterface;
use Mezzio\Session\SessionMiddleware;
use Mezzio\Template\TemplateRendererInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class SessionHandler implements RequestHandlerInterface, ConfigAwareInterface
{

    use ConfigAwareTrait;

    public function __construct(
        protected ApplicationServicePluginManager $applicationServicePluginManager,
        protected InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected ?TemplateRendererInterface $template = null
    ) {

    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var SessionInterface $session */
        $session = $request->getAttribute(SessionMiddleware::SESSION_ATTRIBUTE);
        $sessionDump = print_r($session->toArray(), true);

        $params = [];
        $params['clientId'] = $request->getQueryParams()['client_id'] ?? $this->config['test']['clientId'];
        $params['redirectUri'] = $request->getQueryParams()['redirect_uri'] ?? $this->config['test']['redirectUri'];
        $params['responseType'] = $request->getQueryParams()['response_type'] ?? 'code';
        $params['scope'] = $request->getQueryParams()['scope'] ?? $this->config['test']['scope'];
        $codeVerifier = $request->getQueryParams()['code_challenge'] ?? OAuth2Helper::generateCodeVerifier();
        $params['codeChallenge'] = OAuth2Helper::createS256CodeChallenge($codeVerifier);
        $params['codeChallengeMethod'] = $request->getQueryParams()['code_challenge_method'] ?? 'S256';
        $params['state'] = $request->getQueryParams()['state'] ?? null;
        $params['queryParams'] = http_build_query(
            ArrayHelper::convertKeyLetterCase(
                $params,
                [LetterCaseHelper::LETTER_CASE_SNAKE, LetterCaseHelper::LETTER_CASE_LOWER]
            )
        );
        $params['authProviderBaseUrl'] = $request->getQueryParams()['auth_provider_base_url'] ?? $this->config['test']['authProviderBaseUrl'];
        $params['codeVerifier'] = $codeVerifier;
        $params['codeVerifierHtmlEscaped'] = htmlentities($codeVerifier);
        $params['codeChallengeUrlSafe'] = OAuth2Helper::createS256CodeChallenge($codeVerifier, true);
        $params['layout'] = (new ViewModel(['title' => 'Test Session']))->setTemplate('layout::bootstrap4-default');
        $params['sessionDump'] = $sessionDump;

        return new HtmlResponse(
            $this->template->render(
                'smtm-auth-page::test/session/content',
                $params
            )
        );
    }
}
