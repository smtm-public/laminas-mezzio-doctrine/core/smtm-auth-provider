<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\Token\Http\Handler;

use Smtm\Auth\Context\Token\Domain\TokenInterface;
use Smtm\AuthProvider\Application\Exception\InvalidAuthProviderException;
use Smtm\AuthProvider\Application\Exception\UserNotFoundException;
use Smtm\AuthProvider\Application\Service\Exception\InvalidAuthCodeException;
use Smtm\AuthProvider\Context\Token\Application\Extractor\TokenExtractor;
use Smtm\AuthProvider\Context\Token\Application\Service\Exception\AuthProviderResponseException;
use Smtm\AuthProvider\Context\Token\Application\Service\TokenService;
use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\Http\Exception\BadRequestException;
use Smtm\Base\Http\Exception\InvalidArgumentException;
use Smtm\Base\Http\Exception\UnauthorizedException;
use Smtm\Base\Http\Handler\DbServiceEntity\AbstractCreateHandler;
use Smtm\Base\Http\Middleware\ServerParamsAndRequestIdMiddleware;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Smtm\Base\Infrastructure\Helper\OAuth2Helper;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CreateHandler extends AbstractCreateHandler
{
    public ?string $applicationServiceName = TokenService::class;
    public ?string $domainObjectExtractorName = TokenExtractor::class;
    /** @var TokenService  */
    protected ?AbstractApplicationService $applicationService;

    /**
     * @SWG\Post(path="/experimental/sso/sso/token",
     *   tags={"Token"},
     *   summary="Creates an SSO Token from auth code and return it back for further authentication",
     *   description="Creates access token by given authCode OR refreshToken",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *   @SWG\Response(
     *     response=201,
     *     description="Successfully saved data",
     *     @SWG\Schema(
     *       @SWG\Property(
     *              property="accessToken",
     *              type="string"
     *       ),
     *       @SWG\Property(
     *              property="refreshToken",
     *              type="string"
     *       ),
     *     )
     *   ),
     *   @SWG\Response(response=409, description="Invalid data"),
     *   security={{"Bearer":{}}}
     * )
     */
    public function handle(ServerRequestInterface $request): JsonResponse
    {
        try {
            $parsedBody = $request->getParsedBody();
            $entity = null;

            if ($parsedBody['grantType'] === OAuth2Helper::TOKEN_REQUEST_GRANT_TYPE_AUTHORIZATION_CODE) {
                /** @var TokenInterface $entity */
                $entity = $this->applicationService->createUsingAuthProvider(
                    $request->getParsedBody(),
                    null,
                    ServerParamsAndRequestIdMiddleware::getRequestParams($request)
                );
            } elseif ($parsedBody['grantType'] === OAuth2Helper::TOKEN_REQUEST_GRANT_TYPE_REFRESH_TOKEN) {
                /** @var TokenInterface $entity */
                $entity = $this->applicationService->refreshUsingAuthProvider(
                    $request->getParsedBody(),
                    null,
                    ServerParamsAndRequestIdMiddleware::getRequestParams($request)
                );
            } else {
                throw new BadRequestException(previous: new InvalidArgumentException('Invalid grant type'));
            }

            return $this->prepareResponse(
                $entity,
                static::parseQueryParams($request->getQueryParams()),
                HttpHelper::STATUS_CODE_CREATED
            );
        } catch (AuthProviderResponseException | UserNotFoundException | InvalidAuthCodeException $e) {
            throw new UnauthorizedException(previous: $e);
        } catch (InvalidAuthProviderException $e) {
            throw new BadRequestException('Unknown auth provider', 0, $e);
        }
    }
}
