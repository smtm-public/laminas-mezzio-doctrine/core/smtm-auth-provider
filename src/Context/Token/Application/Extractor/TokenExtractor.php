<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\Token\Application\Extractor;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;
use Smtm\Base\Application\Extractor\Strategy\DateTimeExtractionStrategy;
use Smtm\Base\Infrastructure\Helper\DateTimeHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TokenExtractor extends AbstractDomainObjectExtractor
{
    protected array $properties = [
        'idToken' => null,
        'accessToken' => null,
        'refreshToken' => null,
        'created' => [
            'strategy' => [
                'name' => DateTimeExtractionStrategy::class,
                'options' => [
                    DateTimeExtractionStrategy::OPTION_KEY_FORMAT => DateTimeHelper::DEFAULT_FORMAT,
                ],
            ],
        ],
        'expires' => [
            'strategy' => [
                'name' => DateTimeExtractionStrategy::class,
                'options' => [
                    DateTimeExtractionStrategy::OPTION_KEY_FORMAT => DateTimeHelper::DEFAULT_FORMAT,
                ],
            ],
        ],
        /*
        'tokenType' => null,
        'scope' => null,
        'user' => [
            'strategy' => [
                'name' => DomainObjectExtractionStrategy::class,
                'options' => [
                    DomainObjectExtractionStrategy::OPTION_KEY_EXTRACTOR_NAME => UserExtractor::class,
                ],
            ],
        ],
        */
    ];
}
