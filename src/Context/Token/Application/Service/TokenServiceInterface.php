<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\Token\Application\Service;

use Smtm\Base\Application\Service\ApplicationServiceInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface TokenServiceInterface extends ApplicationServiceInterface
{

}
