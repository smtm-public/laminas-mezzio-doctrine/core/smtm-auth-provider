<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\Token\Application\Service\Exception;

use RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InvalidRefreshTokenException extends RuntimeException
{

}
