<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\Token\Application\Service\Exception;

use Smtm\Base\Application\Exception\RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthProviderResponseException extends RuntimeException
{

}
