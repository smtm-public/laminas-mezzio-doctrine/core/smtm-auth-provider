<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\Token\Application\Service;

use Smtm\Auth\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Auth\Context\Client\Application\Service\ClientService;
use Smtm\Auth\Context\Client\Application\Service\Exception\InvalidClientException;
use Smtm\Auth\Context\Client\Domain\Client;
use Smtm\Auth\Context\Token\Domain\TokenInterface;
use Smtm\Auth\Context\User\Exception\UserNotFoundException;
use Smtm\AuthProvider\Application\Exception\InvalidAuthProviderException;
use Smtm\AuthProvider\Application\Service\AuthProviderServicePluginManagerAwareInterface;
use Smtm\AuthProvider\Application\Service\AuthProviderServicePluginManagerAwareTrait;
use Smtm\AuthProvider\Context\Token\Application\Hydrator\TokenHydrator;
use Smtm\AuthProvider\Context\Token\Application\Service\Exception\AuthProviderResponseException;
use Smtm\AuthProvider\Context\Token\Domain\Token;
use Smtm\Base\Application\Exception\RuntimeException;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceTrait;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityNotFoundException;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Domain\EntityInterface;
use Smtm\Base\Infrastructure\Helper\OAuth2Helper;
use Smtm\Base\Infrastructure\Service\JwtService as TokenInfrastructureService;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceResponseException;
use JetBrains\PhpStorm\ArrayShape;
use Lcobucci\JWT\UnencryptedToken;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TokenService extends AbstractDbService implements
    TokenServiceInterface,
    ConfigAwareInterface,
    UuidAwareEntityDbServiceInterface,
    AuthProviderServicePluginManagerAwareInterface
{

    use ConfigAwareTrait, UuidAwareEntityDbServiceTrait, AuthProviderServicePluginManagerAwareTrait;

    public const TOKEN_EXPIRATION_PERIOD_SECONDS = 3600;

    protected ?string $domainObjectName = Token::class;
    protected ?string $hydratorName = TokenHydrator::class;

    public function createUsingAuthProvider(
        array $data,
        ?EntityInterface $entity = null,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): Token {
        return $this->transactional(
            function () use ($data, $entity, $options) {
                /** @var ClientService $clientService */
                $clientService = $this->applicationServicePluginManager->get(ClientService::class);

                try {
                    /** @var Client $client */
                    $client = $clientService->getOneByClientId($data['clientId']);
                } catch (EntityNotFoundException $e) {
                    throw new InvalidClientException('Client not found.', 0, $e);
                }

                $token = null;

                try {
                    if (!empty($data['providerUuid']) && $this->authProviderServicePluginManager->isAuthProviderRegistered($data['providerUuid'])) {
                        $authProviderService = $this->authProviderServicePluginManager->getInstanceByUuid($data['providerUuid']);
                        $token = $authProviderService->createToken(
                            $client,
                            $data,
                            $token,
                            $options
                        );
                    } else {
                        throw new InvalidAuthProviderException('Invalid Auth Provider with UUID (' . $data['providerUuid'] . ')');
                    }
                } catch (RemoteServiceResponseException $e) {
                    throw new AuthProviderResponseException('', 0, $e);
                } catch (EntityNotFoundException $e) {
                    throw new UserNotFoundException('', 0, $e);
                }

                return $token;
            }
        );
    }

    public function refreshUsingAuthProvider(
        array $data,
        ?EntityInterface $entity = null,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): Token {
        return $this->transactional(
            function () use ($data, $entity, $options) {
                $clientId = $data['clientId'];
                $clientSecret = $data['clientSecret'];
                $refreshToken = $data['refreshToken'];

                /** @var Token $token */
                $token = $this->getOneBy(['token.refreshToken' => $refreshToken]);
                $providerUuid = $token->getToken()->getProviderUuid();
                $token = null;

                /** @var ClientService $clientService */
                $clientService = $this->applicationServicePluginManager->get(ClientService::class);

                try {
                    /** @var Client $client */
                    $client = $clientService->getOneByClientId($clientId);
                } catch (EntityNotFoundException $e) {
                    throw new InvalidClientException('Client not found.', 0, $e);
                }

                if (!$client->verifyClientSecret($clientSecret)) {
                    throw new RuntimeException('The client secret does not match.');
                }

                try {
                    if (!empty($providerUuid) && $this->authProviderServicePluginManager->isAuthProviderRegistered($providerUuid)) {
                        $authProviderService = $this->authProviderServicePluginManager->getInstanceByUuid($providerUuid);
                        $token = $authProviderService->refreshToken(
                            $client,
                            $data + [
                                'client' => $client,
                                'grantType' => OAuth2Helper::TOKEN_REQUEST_GRANT_TYPE_REFRESH_TOKEN
                            ],
                            $token,
                            $options
                        );
                    } else {
                        throw new InvalidAuthProviderException('Invalid Auth Provider with UUID (' . $providerUuid . ')');
                    }
                } catch (RemoteServiceResponseException $e) {
                    throw new AuthProviderResponseException('', 0, $e);
                } catch (EntityNotFoundException $e) {
                    throw new UserNotFoundException('', 0, $e);
                }

                /** @var Token $entity */
                $entity = $this->prepareDomainObject();
                $token = $this->buildToken($client, $entity, $token);

                $tokenData = [];
                $tokenData['token'] = $token;
                $tokenData['idToken'] = $token->toString();
                $now = new \DateTimeImmutable();
                $expires = \DateTimeImmutable::createFromInterface($now)->modify(
                    '+' . static::TOKEN_EXPIRATION_PERIOD_SECONDS. ' seconds'
                );
                $tokenData['expires'] = $expires;

                return parent::create($tokenData, $entity, $options);
            }
        );
    }

    public function revokeAllForUserId(
        int $userId,
        array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): void {
        $this->transactional(
            function () use ($userId, $options) {
                $tokens = $this->getAll([
                    'userClient.user.id' => $userId,
                    'revoked' => false,
                ]);

                /** @var Token $token */
                foreach ($tokens as $token) {
                    parent::update($token, ['revoked' => true], $options);
                }
            }
        );
    }

    public function buildToken(
        Client $client,
        TokenInterface $entity,
        \Smtm\AuthConsumer\Context\Token\Domain\Token $token
    ): UnencryptedToken {
        /** @var TokenInfrastructureService $tokenInfrastructureServiceIssuer */
        $tokenInfrastructureServiceIssuer = $this->infrastructureServicePluginManager->get(
            $this->config['tokenIssuerServiceName']
        );
        $token = $tokenInfrastructureServiceIssuer->build(
            $entity->getUuid(),
            $token->getCreated(),
            $token->getExpires(),
            [
                'uuid' => $token->getUserClient()->getUser()->getUuid(),
                'username' => $token->getUserClient()->getUser()->getUsername(),
                'email' => $token->getUserClient()->getUser()->getEmail(),
                'firstName' => $token->getUserClient()->getUser()->getFirstName(),
                'lastName' => $token->getUserClient()->getUser()->getLastName(),
                'token_type' => OAuth2Helper::TOKEN_TYPE_BEARER,
                'scope' => null,
                'roleCollection' => $token->getUserClient()->getUser()->getRoleCodeCollectionForClient($client),
            ]
        );

        return $token;
    }
}
