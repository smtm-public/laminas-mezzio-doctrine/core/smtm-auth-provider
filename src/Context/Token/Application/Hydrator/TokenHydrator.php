<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\Token\Application\Hydrator;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TokenHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'token' => 'You must specify the token of the Token.',
        'idToken' => 'You must specify an accessToken for the Token.',
    ];
}
