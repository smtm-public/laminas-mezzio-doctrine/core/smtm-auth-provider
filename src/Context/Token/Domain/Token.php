<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\Token\Domain;

use DateTimeImmutable;
use Smtm\Auth\Context\Token\Domain\TokenInterface;
use Smtm\Auth\Context\UserClient\Domain\UserClient;
use Smtm\AuthConsumer\Context\Token\Domain\Token as ConsumerToken;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\IdAwareEntityTrait;
use Smtm\Base\Domain\MarkedForUpdateInterface;
use Smtm\Base\Domain\MarkedForUpdateTrait;
use Smtm\Base\Domain\NotArchivedAwareEntityTrait;
use Smtm\Base\Domain\UuidAwareEntityTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Token implements TokenInterface, MarkedForUpdateInterface
{

    use IdAwareEntityTrait,
        UuidAwareEntityTrait,
        CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait,
        NotArchivedAwareEntityTrait,
        ArchivedDateTimeAwareEntityTrait,
        MarkedForUpdateTrait;

    protected string $providerUuid;
    protected UserClient $userClient;
    protected ?ConsumerToken $token = null;
    protected ?string $idToken = null;
    protected string $accessToken;
    protected ?string $refreshToken = null;
    protected DateTimeImmutable $expires;
    protected ?DateTimeImmutable $refreshTokenExpires = null;
    protected ?string $tokenType = null;
    protected ?string $scope = null;
    protected bool $used = false;
    protected bool $revoked = false;

    public function getProviderUuid(): string
    {
        return $this->providerUuid;
    }

    public function setProviderUuid(string $providerUuid): static
    {
        return $this->__setProperty('providerUuid', $providerUuid);
    }

    public function getUserClient(): UserClient
    {
        return $this->userClient;
    }

    public function setUserClient(UserClient $userClient): static
    {
        return $this->__setProperty('userClient', $userClient);
    }

    public function getToken(): ?ConsumerToken
    {
        return $this->token;
    }

    public function setToken(?ConsumerToken $token): static
    {
        return $this->__setProperty('token', $token);
    }

    public function getIdToken(): ?string
    {
        return $this->idToken;
    }

    public function setIdToken(?string $idToken): static
    {
        return $this->__setProperty('idToken', $idToken);
    }

    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    public function setAccessToken(string $accessToken): static
    {
        return $this->__setProperty('accessToken', $accessToken);
    }

    public function getRefreshToken(): ?string
    {
        return $this->refreshToken;
    }

    public function setRefreshToken(?string $refreshToken): static
    {
        return $this->__setProperty('refreshToken', $refreshToken);
    }

    public function getExpires(): DateTimeImmutable
    {
        return $this->expires;
    }

    public function setExpires(DateTimeImmutable $expires): static
    {
        return $this->__setProperty('expires', $expires);
    }

    public function getRefreshTokenExpires(): ?DateTimeImmutable
    {
        return $this->refreshTokenExpires;
    }

    public function setRefreshTokenExpires(?DateTimeImmutable $refreshTokenExpires): static
    {
        return $this->__setProperty('refreshTokenExpires', $refreshTokenExpires);
    }

    public function getTokenType(): ?string
    {
        return $this->tokenType;
    }

    public function setTokenType(?string $tokenType): static
    {
        return $this->__setProperty('tokenType', $tokenType);
    }

    public function getScope(): ?string
    {
        return $this->scope;
    }

    public function setScope(?string $scope): static
    {
        return $this->__setProperty('scope', $scope);
    }

    public function getUsed(): bool
    {
        return $this->used;
    }

    public function setUsed(bool $used): static
    {
        return $this->__setProperty('used', $used);
    }

    public function getRevoked(): bool
    {
        return $this->revoked;
    }

    public function setRevoked(bool $revoked): static
    {
        return $this->__setProperty('revoked', $revoked);
    }
}
