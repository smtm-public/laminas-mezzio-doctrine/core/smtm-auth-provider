<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\Token\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TokenRepository extends AbstractRepository implements TokenRepositoryInterface
{

}
