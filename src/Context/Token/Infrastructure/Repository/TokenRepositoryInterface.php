<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Context\Token\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Repository\RepositoryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface TokenRepositoryInterface extends RepositoryInterface
{

}
