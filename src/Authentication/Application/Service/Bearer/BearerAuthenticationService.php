<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Authentication\Application\Service\Bearer;

use Smtm\Auth\Authentication\Application\Service\Bearer\BearerAuthenticationServiceInterface;
use Smtm\Auth\Authentication\Application\Service\Bearer\Exception\InvalidAccessTokenException;
use Smtm\Auth\Authentication\Application\Service\Bearer\Exception\InvalidIdTokenException;
use Smtm\Auth\Authentication\Application\Service\Bearer\Exception\InvalidJwtSignatureException;
use Smtm\Auth\Authentication\Application\Service\Bearer\Exception\TokenNotFoundException;
use Smtm\Auth\Context\User\Domain\UserInterface;
use Smtm\Auth\Context\UserClient\Domain\UserClientInterface;
use Smtm\AuthProvider\Application\Service\AuthProviderServiceInterface;
use Smtm\AuthProvider\Context\Token\Application\Service\TokenService;
use Smtm\AuthProvider\Context\Token\Application\Service\TokenServiceInterface;
use Smtm\AuthProvider\Context\Token\Domain\Token;
use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityNotFoundException;
use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Infrastructure\Service\CryptoService;
use Smtm\Base\Infrastructure\Service\JwtService as JwtInfrastructureService;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class BearerAuthenticationService extends AbstractApplicationService implements
    BearerAuthenticationServiceInterface,
    ConfigAwareInterface
{

    use ConfigAwareTrait;

    protected ?Token $authenticatedToken = null;
    protected ?\Lcobucci\JWT\Token $parsedJwt = null;

    public function authenticateWithToken(
        string $tokenData
    ): void {
        /** @var TokenService $tokenService */
        $tokenService = $this->applicationServicePluginManager->get(TokenServiceInterface::class);
        /** @var JwtInfrastructureService $jwtInfrastructureServiceAuthIssuer */
        $jwtInfrastructureServiceAuthIssuer = $this->infrastructureServicePluginManager->get(
            $this->config[AuthProviderServiceInterface::CONFIG_KEY_JWT_ISSUER_SERVICE_NAME]
        );

        $token = null;
        $parsedIdToken = null;

        if (substr_count($tokenData, '.') === 2) { // the token data is a JWT
            try {
                $parsedIdToken = $jwtInfrastructureServiceAuthIssuer->parse($tokenData);

                if (!$jwtInfrastructureServiceAuthIssuer->validate($parsedIdToken)) {
                    throw new InvalidJwtSignatureException();
                }

                $uuid = $jwtInfrastructureServiceAuthIssuer->getClaimJti($parsedIdToken);
                /** @var Token $token */
                $token = $tokenService->getOneByUuid($uuid);
            } catch (EntityNotFoundException $e) {
                throw new TokenNotFoundException(previous: $e);
            } catch (\Exception $e) {
                throw new InvalidAccessTokenException(previous: $e);
            }
        } else {
            /** @var CryptoService $cryptoService */
            $cryptoService = $this->infrastructureServicePluginManager->get(CryptoService::class);

            try {
                $tokenDecrypted = $cryptoService->decrypt($tokenData);
                $tokenData = json_decode(
                    $tokenDecrypted,
                    true,
                    JSON_THROW_ON_ERROR
                );
                $uuid = $tokenData['uuid'];
                /** @var Token $token */
                $token = $tokenService->getOneByUuid($uuid);
                $idToken = $token->getIdToken();
                $parsedIdToken = $jwtInfrastructureServiceAuthIssuer->parse($idToken);

                if (!$jwtInfrastructureServiceAuthIssuer->validate($parsedIdToken)) {
                    throw new InvalidJwtSignatureException();
                }
            } catch (EntityNotFoundException $e) {
                throw new TokenNotFoundException(previous: $e);
            } catch (\Exception $e) {
                throw new InvalidIdTokenException(previous: $e);
            }
        }

        $this->setAuthenticatedToken($token);
        $this->setParsedJwt($parsedIdToken);
    }

    public function getAuthenticatedUserClient(): UserClientInterface
    {
        return $this->authenticatedToken->getUserClient();
    }

    public function getAuthenticatedUser(): UserInterface
    {
        return $this->authenticatedToken->getUserClient()->getUser();
    }

    public function getRoleCodeCollection(): array
    {
        return $this->authenticatedToken->getUserClient()->getRoleCodeCollection();
    }

    public function getAuthenticatedToken(): ?Token
    {
        return $this->authenticatedToken;
    }

    public function setAuthenticatedToken(?Token $authenticatedToken): static
    {
        $this->authenticatedToken = $authenticatedToken;

        return $this;
    }

    public function getParsedJwt(): ?\Lcobucci\JWT\Token
    {
        return $this->parsedJwt;
    }

    public function setParsedJwt(?\Lcobucci\JWT\Token $parsedJwt): static
    {
        $this->parsedJwt = $parsedJwt;

        return $this;
    }
}
