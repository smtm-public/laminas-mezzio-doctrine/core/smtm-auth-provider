<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Application\Exception;

use Smtm\Auth\Context\Client\Domain\ClientInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UserNotFoundException extends \Smtm\Auth\Context\User\Exception\UserNotFoundException
{
    public const MESSAGE = 'USER_NOT_FOUND';
    public const CODE = 0x100001;

    protected $message = self::MESSAGE;
    protected $code = self::CODE;

    protected ClientInterface $client;

    public function getClient(): ClientInterface
    {
        return $this->client;
    }

    public function setClient(ClientInterface $client): static
    {
        $this->client = $client;

        return $this;
    }
}
