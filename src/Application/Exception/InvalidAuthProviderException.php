<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Application\Exception;

use RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InvalidAuthProviderException extends RuntimeException
{
    public const MESSAGE = 'Invalid Auth Provider';
    public const CODE = 0;

    protected $message = self::MESSAGE;
    protected $code = self::CODE;

    public function __construct(string $message = null, int $code = null, ?\Throwable $previous = null)
    {
        parent::__construct($message ?? static::MESSAGE, $code ?? static::CODE, $previous);
    }
}
