<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Application\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait AuthProviderServicePluginManagerAwareTrait
{
    protected AuthProviderServicePluginManager $authProviderServicePluginManager;

    public function getAuthProviderServicePluginManager(): AuthProviderServicePluginManager
    {
        return $this->authProviderServicePluginManager;
    }

    public function setAuthProviderServicePluginManager(AuthProviderServicePluginManager $authProviderServicePluginManager): static
    {
        $this->authProviderServicePluginManager = $authProviderServicePluginManager;

        return $this;
    }
}
