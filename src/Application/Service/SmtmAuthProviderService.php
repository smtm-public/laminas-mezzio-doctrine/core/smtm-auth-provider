<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Application\Service;

use Smtm\Auth\Context\Client\Domain\ClientInterface;
use Smtm\Auth\Context\Token\Domain\TokenInterface;
use Smtm\AuthProvider\Application\Service\Exception\InvalidAuthCodeException;
use Smtm\AuthProvider\Application\Service\Exception\InvalidClientSecretException;
use Smtm\AuthProvider\Context\AuthCode\Application\Service\AuthCodeService;
use Smtm\AuthProvider\Context\AuthCode\Domain\AuthCode;
use Smtm\AuthProvider\Context\Token\Application\Service\TokenService;
use Smtm\AuthProvider\Context\Token\Application\Service\TokenServiceInterface;
use Smtm\AuthProvider\Context\Token\Domain\Token;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityNotFoundException;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Http\Exception\UnauthorizedException;
use Smtm\Base\Infrastructure\Exception\InvalidArgumentException;
use Smtm\Base\Infrastructure\Helper\OAuth2Helper;
use Smtm\Base\Infrastructure\Service\CryptoService;
use Smtm\Base\Infrastructure\Service\JwtService;
use Smtm\Base\Infrastructure\Service\OAuth2Service;
use Smtm\Base\OptionsAwareInterface;
use Smtm\Base\OptionsAwareTrait;
use JetBrains\PhpStorm\ArrayShape;
use Lcobucci\JWT\Token\RegisteredClaims;
use Lcobucci\JWT\UnencryptedToken;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class SmtmAuthProviderService extends AbstractAuthProviderService implements ConfigAwareInterface, OptionsAwareInterface
{

    use ConfigAwareTrait, OptionsAwareTrait;

    public const UUID = 'ef58751b-7ebb-4a21-90d9-dfdaca81adf6';

    public function __construct(?array $options = [])
    {
        $this->setOptions($options ?? []);
    }

    public static function getUuid(): string
    {
        return static::UUID;
    }

    public static function getExpiresDateTime(\DateTimeInterface $reference, int $expiresInSeconds): \DateTimeImmutable
    {
        return \DateTimeImmutable::createFromInterface($reference)
            ->modify('+' . $expiresInSeconds . ' seconds');
    }

    public function createToken(
        ClientInterface $client,
        array $data,
        ?TokenInterface $entity = null,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): TokenInterface {
        if (!$client->verifyClientSecret($data['clientSecret'])) {
            throw new InvalidClientSecretException();
        }

        /** @var CryptoService $cryptoService */
        $cryptoService = $this->infrastructureServicePluginManager->get(CryptoService::class);

        try {
            $authCodeDecrypted = $cryptoService->decrypt($data['code']);
            $authCodeData = json_decode(
                $authCodeDecrypted,
                true,
                JSON_THROW_ON_ERROR
            );
        } catch (\Exception $e) {
            throw new InvalidAuthCodeException('', 0, $e);
        }

        if (empty($authCodeData['uuid']) || empty($authCodeData['salt'])) {
            throw new InvalidAuthCodeException();
        }

        /** @var AuthCodeService $authCodeService */
        $authCodeService = $this->applicationServicePluginManager->get(AuthCodeService::class);

        try {
            /** @var AuthCode $authCode */
            $authCode = $authCodeService->getOneByUuid($authCodeData['uuid']);
        } catch (EntityNotFoundException $e) {
            throw new InvalidAuthCodeException('', 0, $e);
        }

        if ($authCode->getCode() !== $data['code']
            || !$authCode->getUserClient()->getUser()->hasClient($client)
        ) {
            throw new InvalidAuthCodeException();
        }

        if ($authCode->getRedirectUri() !== $data['redirectUri']) {
            throw new InvalidAuthCodeException('Invalid redirect_uri');
        }

        try {
            OAuth2Helper::verifyCodeChallenge(
                $authCode->getCodeChallenge(),
                $authCode->getCodeChallengeMethod(),
                $data['codeVerifier']
            );
        } catch (InvalidArgumentException $e) {
            throw new InvalidAuthCodeException($e->getMessage(), 0, $e);
        }


        /** @var TokenService $tokenService */
        $tokenService = $this->applicationServicePluginManager->get(TokenServiceInterface::class);

        /** @var JwtService $jwtInfrastructureServiceIssuer */
        $jwtInfrastructureServiceIssuer = $this->infrastructureServicePluginManager->get(
            $this->config[AuthProviderServiceInterface::CONFIG_KEY_JWT_ISSUER_SERVICE_NAME]
        );
        $entityData = [];
        /** @var Token $entity */
        $entity = $tokenService->prepareDomainObject();
        $entityData['providerUuid'] = $data['providerUuid'];
        $entityData['userClient'] = $authCode->getUserClient();
        $now = new \DateTimeImmutable();
        $jwt = $this->buildJwt(
            $entity->getUuid(),
            $now,
            $authCode->getUserClient()->getUser()->getUuid(),
            $authCode->getUserClient()->getUser()->getUsername(),
            $authCode->getUserClient()->getUser()->getEmail(),
            $authCode->getUserClient()->getUser()->getFirstName(),
            $authCode->getUserClient()->getUser()->getLastName(),
            $authCode->getUserClient()->getUser()->getGenderIso5218(),
            $authCode->getScope(),
            $authCode->getUserClient()->getRoleCodeCollection(),
        );

        // TODO: MD - replace with the standard expires_in
        $entityData['expires'] = $jwt->claims()->get(RegisteredClaims::EXPIRATION_TIME);
        $refreshTokenExpires = static::getExpiresDateTime(
            $now,
            $this->config[AuthProviderServiceInterface::CONFIG_KEY_REFRESH_TOKEN_EXPIRATION_SECONDS]
        );
        $entityData['refreshTokenExpires'] = $refreshTokenExpires;
        $entityData['tokenType'] = OAuth2Helper::TOKEN_TYPE_BEARER;
        $entityData['scope'] = $authCode->getScope();
        $entityData['idToken'] = $jwt->toString();
        /** @var OAuth2Service $oAuth2Service */
        $oAuth2Service = $this->infrastructureServicePluginManager->get(OAuth2Service::class);
        $entityData['accessToken'] = $oAuth2Service->generateAccessToken(
            $entityData + ['uuid' => $entity->getUuid()]
        );
        $entityData['refreshToken'] = $oAuth2Service->generateRefreshToken(
            $entityData + ['uuid' => $entity->getUuid()]
        );

        $token = $tokenService->create($entityData, $entity, $options);

        $authCodeService->update($authCode, ['used' => true], $options);

        return $token;
    }

    public function refreshToken(
        ClientInterface $client,
        array $data,
        ?TokenInterface $entity = null,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): TokenInterface {
        /** @var TokenServiceInterface $tokenService */
        $tokenService = $this->applicationServicePluginManager->get(TokenServiceInterface::class);

        try {
            /** @var TokenInterface $token */
            $token = $tokenService->create(
                $data + [
                    'client' => $client,
                    'grantType' => OAuth2Helper::TOKEN_REQUEST_GRANT_TYPE_REFRESH_TOKEN
                ],
                null,
                $options
            );
        } catch (\Smtm\AuthProvider\Context\Token\Application\Service\Exception\InvalidAuthCodeException $e) {
            throw new UnauthorizedException(previous: $e);
        }

        return $token;
    }

    public static function buildJwtFromParams(
        JwtService $jwtInfrastructureServiceIssuer,
        \DateTimeInterface $issuedDateTime,
        int $tokenExpirationSeconds,
        string $tokenUuid,
        string $userUuid,
        string $userUsername,
        string $userEmail,
        string $userFirstName,
        string $userLastName,
        int $userGenderIso5218,
        string $authCodeScope,
        array $roleCodeCollection
    ): UnencryptedToken {
        $expires = static::getExpiresDateTime($issuedDateTime, $tokenExpirationSeconds);
        $jwt = $jwtInfrastructureServiceIssuer->build(
            $tokenUuid,
            \DateTimeImmutable::createFromInterface($issuedDateTime),
            $expires,
            [
                'uuid' => $userUuid,
                'username' => $userUsername,
                'email' => $userEmail,
                'firstName' => $userFirstName,
                'lastName' => $userLastName,
                'genderIso5218' => $userGenderIso5218,
                'tokenType' => OAuth2Helper::TOKEN_TYPE_BEARER,
                'scope' => $authCodeScope,
                'roleCollection' => $roleCodeCollection,
            ]
        );

        return $jwt;
    }

    public function buildJwt(
        string $tokenUuid,
        \DateTimeInterface $issuedDateTime,
        string $userUuid,
        string $userUsername,
        string $userEmail,
        string $userFirstName,
        string $userLastName,
        int $userGenderIso5218,
        string $authCodeScope,
        array $roleCodeCollection,
        #[ArrayShape([
            AuthProviderServiceInterface::CONFIG_KEY_TOKEN_EXPIRATION_SECONDS => '?int',
            AuthProviderServiceInterface::CONFIG_KEY_REFRESH_TOKEN_EXPIRATION_SECONDS => '?int',
            AuthProviderServiceInterface::CONFIG_KEY_JWT_ISSUER_SERVICE_NAME => '?string',
        ])] ?array $configOverrides = []
    ): UnencryptedToken {
        return static::buildJwtFromParams(
            $this->infrastructureServicePluginManager->get(
                $configOverrides[AuthProviderServiceInterface::CONFIG_KEY_JWT_ISSUER_SERVICE_NAME]
                ?? $this->config[AuthProviderServiceInterface::CONFIG_KEY_JWT_ISSUER_SERVICE_NAME]
            ),
            $issuedDateTime,
            $configOverrides[AuthProviderServiceInterface::CONFIG_KEY_TOKEN_EXPIRATION_SECONDS]
            ?? $this->config[AuthProviderServiceInterface::CONFIG_KEY_TOKEN_EXPIRATION_SECONDS],
            $tokenUuid,
            $userUuid,
            $userUsername,
            $userEmail,
            $userFirstName,
            $userLastName,
            $userGenderIso5218,
            $authCodeScope,
            $roleCodeCollection
        );
    }

    public function buildToken(
        ClientInterface $client,
        TokenInterface $entity,
        \Smtm\AuthConsumer\Context\Token\Domain\Token $token
    ): UnencryptedToken {
        /** @var TokenInfrastructureService $tokenInfrastructureServiceIssuer */
        $tokenInfrastructureServiceIssuer = $this->infrastructureServicePluginManager->get(
            $this->config['tokenIssuerServiceName']
        );
        $token = $tokenInfrastructureServiceIssuer->build(
            $entity->getUuid(),
            $token->getCreated(),
            $token->getExpires(),
            [
                'uuid' => $token->getUserClient()->getUser()->getUuid(),
                'username' => $token->getUserClient()->getUser()->getUsername(),
                'email' => $token->getUserClient()->getUser()->getEmail(),
                'firstName' => $token->getUserClient()->getUser()->getFirstName(),
                'lastName' => $token->getUserClient()->getUser()->getLastName(),
                'token_type' => OAuth2Helper::TOKEN_TYPE_BEARER,
                'scope' => null,
                'roleCollection' => $token->getUserClient()->getUser()->getRoleCodeCollectionForClient($client),
            ]
        );

        return $token;
    }
}
