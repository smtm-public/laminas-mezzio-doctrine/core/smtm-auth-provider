<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Application\Service;

use Laminas\ServiceManager\AbstractPluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthProviderServicePluginManager extends AbstractPluginManager
{
    protected $instanceOf = AuthProviderServiceInterface::class;
    protected array $registeredAuthProviders = [];

    public function registerAuthProvider(
        string $uuid,
        string $name
    ): void {
        $this->registeredAuthProviders[$uuid] = $name;
    }

    public function isAuthProviderRegistered(
        string $uuid
    ): bool {
        return array_key_exists($uuid, $this->registeredAuthProviders);
    }

    public function getInstanceByUuid(string $uuid): AbstractAuthProviderService
    {
        return $this->get($this->registeredAuthProviders[$uuid]);
    }
}
