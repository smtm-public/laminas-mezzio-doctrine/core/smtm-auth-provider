<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Application\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface AuthProviderServicePluginManagerAwareInterface
{
    public function getAuthProviderServicePluginManager(): AuthProviderServicePluginManager;
    public function setAuthProviderServicePluginManager(AuthProviderServicePluginManager $authProviderServicePluginManager): static;
}
