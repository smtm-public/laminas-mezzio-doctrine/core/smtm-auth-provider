<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Application\Service\Exception;

use RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InvalidAuthCodeException extends RuntimeException
{

}
