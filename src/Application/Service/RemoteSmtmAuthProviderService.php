<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Application\Service;

use Smtm\Auth\Authentication\Application\Service\AuthenticationService;
use Smtm\Auth\Authentication\Application\Service\Bearer\BearerAuthenticationServiceInterface;
use Smtm\Auth\Context\Client\Domain\ClientInterface;
use Smtm\Auth\Context\Role\Application\Service\RoleService;
use Smtm\Auth\Context\Role\Application\Service\RoleServiceInterface;
use Smtm\Auth\Context\Token\Domain\TokenInterface;
use Smtm\Auth\Context\User\Application\Service\UserService;
use Smtm\Auth\Context\User\Application\Service\UserServiceInterface;
use Smtm\Auth\Context\User\Domain\User;
use Smtm\Auth\Context\User\Domain\UserInterface;
use Smtm\Auth\Context\User\Exception\UserNotFoundException;
use Smtm\Auth\Context\UserClient\Application\Service\UserClientService;
use Smtm\Auth\Context\UserClientRole\Application\Service\UserClientRoleService;
use Smtm\AuthConsumer\Context\Token\Application\Service\TokenService as ConsumerTokenService;
use Smtm\AuthConsumer\Context\Token\Domain\Token as ConsumerToken;
use Smtm\AuthProvider\Application\Service\Exception\InvalidAuthCodeException;
use Smtm\AuthProvider\Context\Token\Application\Service\Exception\AuthProviderResponseException;
use Smtm\AuthProvider\Context\Token\Application\Service\TokenService as ProviderTokenService;
use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityNotFoundException;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Http\Exception\UnauthorizedException;
use Smtm\Base\Infrastructure\Helper\GenderIso5218Helper;
use Smtm\Base\Infrastructure\Helper\OAuth2Helper;
use Smtm\Base\Infrastructure\Service\JwtService;
use Smtm\Base\Infrastructure\Service\OAuth2Service;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceResponseException;
use Smtm\Base\OptionsAwareInterface;
use Smtm\Base\OptionsAwareTrait;
use Smtm\Smtm\Context\User\Context\OAuthToken\Application\Service\SmtmService;
use JetBrains\PhpStorm\ArrayShape;
use Lcobucci\JWT\Token\RegisteredClaims;
use Lcobucci\JWT\UnencryptedToken;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RemoteSmtmAuthProviderService extends AbstractAuthProviderService implements ConfigAwareInterface, OptionsAwareInterface
{

    use ConfigAwareTrait, OptionsAwareTrait;

    public const UUID = '74cf97f1-9d0d-43b3-b038-83761e62d29e';

    public function __construct(?array $options = [])
    {
        $this->setOptions($options ?? []);
    }

    public static function getUuid(): string
    {
        return static::UUID;
    }

    public function createToken(
        ClientInterface $client,
        array $data,
        ?TokenInterface $entity = null,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): TokenInterface {
        $responseJwt = null;

        try {
            /** @var SmtmService $smtmService */
            $smtmService = $this->applicationServicePluginManager->get(SmtmService::class);
            $tokenData = $smtmService->createTokenDataFromAuthCode(
                SmtmAuthProviderService::getUuid(),
                $data['code'],
                $data['redirectUri'],
                $data['codeVerifier'],
                [
                    AbstractApplicationService::OPTION_NAME_LOG_EXTRA_DATA =>
                        [
                            'script' => __FILE__,
                            'service_name' => 'smtm-sso',
                        ],
                ]
            );
            /** @var JwtService $jwtInfrastructureServiceSsoVerifier */
            $jwtInfrastructureServiceSsoVerifier = $this->infrastructureServicePluginManager->get(
                $this->config[AuthProviderServiceInterface::CONFIG_KEY_JWT_VERIFIER_SERVICE_NAME]
            );
            $responseJwt = $jwtInfrastructureServiceSsoVerifier->parse($tokenData['idToken']);
            /** @var UserService $userService */
            $userService = $this->applicationServicePluginManager->get(
                UserServiceInterface::class
            );
            $user = $userService->getOneOrNullByRemoteUuid($responseJwt->claims()->get('uuid'));

            if ($user === null) {
                $user = $userService->getOneOrNullByEmail($responseJwt->claims()->get('email'));
            }

            if ($user === null) {
                $user = $userService->getOneOrNullByUsername($responseJwt->claims()->get('username'));
            }

            if ($user === null) {
                if ($this->config['automaticallyCreateAuthenticatedUsers']) {
                    $user = $this->createUser($client, $responseJwt, $options);
                } else {
                    throw new EntityNotFoundException(
                        EntityNotFoundException::formatMessageForEntity($userService->getDomainObjectName())
                    );
                }
            }
        } catch (RemoteServiceResponseException $e) {
            throw new AuthProviderResponseException('', 0, $e);
        } catch (EntityNotFoundException $e) {
            throw new UserNotFoundException('', 0, $e);
        }

        $tokenData['providerUuid'] = $data['providerUuid'];
        $user = $this->updateUser($client, $user, $responseJwt);
        /** @var UserClientService $userClientService */
        $userClientService = $this->applicationServicePluginManager->get(UserClientService::class);
        $userClient = $userClientService->getOneBy(
            [
                'user' => $user,
                'client' => $client
            ]
        );
        $tokenData['userClient'] = $userClient;

        /** @var ConsumerTokenService $tokenService */
        $consumerTokenService = $this->applicationServicePluginManager->get(ConsumerTokenService::class);
        $consumerToken = $consumerTokenService->create($tokenData);
        $tokenData['token'] = $consumerToken;

        /** @var ProviderTokenService $tokenService */
        $providerTokenService = $this->applicationServicePluginManager->get(ProviderTokenService::class);
        /** @var ConsumerToken $entity */
        $entity = $providerTokenService->prepareDomainObject();
        $jwt = $this->buildJwt($entity, $responseJwt);

        $tokenData['idToken'] = $jwt->toString();
        $tokenData['expires'] = $responseJwt->claims()->get(RegisteredClaims::EXPIRATION_TIME);
        /** @var OAuth2Service $oAuth2Service */
        $oAuth2Service = $this->infrastructureServicePluginManager->get(OAuth2Service::class);
        $tokenData['accessToken'] = $oAuth2Service->generateAccessToken(
            $tokenData + ['uuid' => $entity->getUuid()]
        );

        try {
            /** @var TokenInterface $providerToken */
            $providerToken = $providerTokenService->create(
                $tokenData + [
                    'grantType' => OAuth2Helper::TOKEN_REQUEST_GRANT_TYPE_AUTHORIZATION_CODE
                ],
                $entity,
                $options
            );
        } catch (InvalidAuthCodeException $e) {
            throw new UnauthorizedException(previous: $e);
        }

        return $providerToken;
    }

    public function refreshToken(
        ClientInterface $client,
        array $data,
        ?TokenInterface $entity = null,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): TokenInterface {
        /** @var AuthenticationService $authenticationService */
        $rootAuthenticationService = $this->applicationServicePluginManager->get(AuthenticationService::class);
        /** @var BearerAuthenticationServiceInterface $authenticationService */
        $authenticationService = $rootAuthenticationService->getAuthenticationService();
        /** @var Jwt $authenticatedJwt */
        $authenticatedJwt = $authenticationService->getAuthenticatedJwt();

        /** @var JwtInfrastructureService $jwtInfrastructureServiceSsoVerifier */
        $jwtInfrastructureServiceSsoVerifier = $this->infrastructureServicePluginManager->get(
            $this->config['ssoVerifierServiceName']
        );
        /** @var UserService $userService */
        $userService = $this->applicationServicePluginManager->get(
            UserServiceInterface::class
        );

        try {
            /** @var SmtmService $smtmService */
            $smtmService = $this->applicationServicePluginManager->get(SmtmService::class);
            $tokenData = $smtmService->createTokenDataFromRefresh(
                $authenticatedJwt->getAccessToken(),
                [
                    AbstractApplicationService::OPTION_NAME_LOG_EXTRA_DATA =>
                        [
                            'script' => __FILE__,
                            'service_name' => 'smtm-sso',
                        ],
                ]
            );
            $responseJwt = $jwtInfrastructureServiceSsoVerifier->parse($tokenData['idToken']);
            $user = $userService->getOneOrNullByRemoteUuid($responseJwt->claims()->get('uuid'));

            if ($user === null) {
                $user = $userService->getOneOrNullByEmail($responseJwt->claims()->get('email'));
            }

            if ($this->config['automaticallyCreateAuthenticatedUsers']) {
                if ($user === null) {
                    $user = $this->createUser($responseJwt, $options);
                }
            } elseif ($user === null) {
                throw new EntityNotFoundException(
                    EntityNotFoundException::formatMessageForEntity($userService->getDomainObjectName())
                );
            }

            $tokenData['user'] = $user;
        } catch (RemoteServiceResponseException $e) {
            throw new AuthenticationProviderResponseException('', 0, $e);
        } catch (EntityNotFoundException $e) {
            throw new UserNotFoundException('', 0, $e);
        }

        /** @var Jwt $entity */
        $entity = $this->prepareDomainObject();
        $jwt = $this->buildJwt($entity, $responseJwt);

        $tokenData['accessToken'] = $jwt->toString();
        $tokenData['expires'] = $responseJwt->claims()->get(RegisteredClaims::EXPIRATION_TIME);

        try {
            /** @var TokenInterface $token */
            $token = $tokenService->create(
                $tokenData + [
                    'client' => $client,
                    'grantType' => OAuth2Helper::TOKEN_REQUEST_GRANT_TYPE_REFRESH_TOKEN
                ],
                $entity,
                $options
            );
        } catch (\Smtm\AuthProvider\Context\Token\Application\Service\Exception\InvalidAuthCodeException $e) {
            throw new UnauthorizedException(previous: $e);
        }

        return $token;
    }

    public function revokeToken(UserInterface $user): void
    {
        /** @var OAuthTokenReaderService $oAuthTokenService */
        $oAuthTokenService = $this->applicationServicePluginManager->get(OAuthTokenReaderService::class);
        /** @var OAuthToken $oAuthToken */
        $oAuthToken = $oAuthTokenService->getOneOrNullBy(
            [ 'user.id' => $user->getId() ],
            orderBy: [ 'id' => 'DESC' ]
        );


        if ($oAuthToken) {
            $authenticatedJwtTokens = $this->getAll([
                'oAuthToken.id' => $oAuthToken->getId()
            ]);

            foreach ($authenticatedJwtTokens as $authenticatedJwtToken) {
                $this->update($authenticatedJwtToken, [
                    'revoked' => true,
                    'expires' => new DateTimeImmutable(),
                ]);
            }
        }
    }

    public function createUser(
        ClientInterface $client,
        UnencryptedToken $responseJwt,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): UserInterface {
        /** @var UserService $userService */
        $userService = $this->applicationServicePluginManager->get(UserServiceInterface::class);
        /** @var User $user */
        $user = $userService->create(
            [
                'remoteUuid' => $responseJwt->claims()->get('uuid'),
                'email' => $responseJwt->claims()->get('email'),
                'firstName' => $responseJwt->claims()->get('firstName'),
                'lastName' => $responseJwt->claims()->get('lastName'),
                'genderIso5218' =>
                    $responseJwt->claims()->has('genderIso5218')
                        ? $responseJwt->claims()->get('genderIso5218')
                        : GenderIso5218Helper::NOT_SPECIFIED,
                'status' => User::STATUS_ACTIVE,
                'createdBy' => $userService->getById(1),
            ],
            null,
            $options
        );
        /** @var UserClientService $userClientService */
        $userClientService = $this->applicationServicePluginManager->get(UserClientService::class);
        $userClient = $userClientService->create(
            [
                'user' => $user,
                'client' => $client
            ]
        );
        /** @var RoleService $roleService */
        $roleService = $this->applicationServicePluginManager->get(RoleServiceInterface::class);
        /** @var UserClientRoleService $userClientRoleService */
        $userClientRoleService = $this->applicationServicePluginManager->get(UserClientRoleService::class);

        foreach ($responseJwt->claims()->get('roleCollection') as $roleCode) {
            $role = $roleService->getOneBy(['code' => $roleCode]);
            $userClientRoleService->create(
                [
                    'userClient' => $userClient,
                    'role' => $role
                ]
            );
        }

        return $user;
    }

    public function updateUser(
        ClientInterface $client,
        UserInterface $user,
        UnencryptedToken $responseJwt,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): UserInterface {
        $userClientForClient = null;

        foreach ($user->getUserClientCollection() as $userClient) {
            if ($userClient->getClient()->getClientId() === $client->getClientId()) {
                $userClientForClient = $userClient;

                break;
            }
        }

        /** @var UserClientService $userClientService */
        $userClientService = $this->applicationServicePluginManager->get(UserClientService::class);

        if ($userClientForClient === null) {
            $userClientForClient = $userClientService->create(
                [
                    'user' => $user,
                    'client' => $client
                ]
            );
        }

        $roleCodeDiff = array_diff($userClientForClient->getRoleCodeCollection(), $responseJwt->claims()->get('roleCollection'));

        /** @var User $user */
        if ($user->getRemoteUuid() !== $responseJwt->claims()->get('uuid')
            || $user->getUsername() !== $responseJwt->claims()->get('username')
            || $user->getEmail() !== $responseJwt->claims()->get('email')
            || $user->getFirstName() !== $responseJwt->claims()->get('firstName')
            || $user->getLastName() !== $responseJwt->claims()->get('lastName')
            || $user->getGenderIso5218() !== $responseJwt->claims()->get('genderIso5218')
            || !empty($roleCodeDiff)
        ) {
            /** @var UserService $userService */
            $userService = $this->applicationServicePluginManager->get(UserServiceInterface::class);
            $user = $userService->update(
                $user,
                [
                    'remoteUuid' => $responseJwt->claims()->get('uuid'),
                    'username' => $responseJwt->claims()->get('username'),
                    'email' => $responseJwt->claims()->get('email'),
                    'firstName' => $responseJwt->claims()->get('firstName'),
                    'lastName' => $responseJwt->claims()->get('lastName'),
                    'genderIso5218' =>
                        $responseJwt->claims()->has('genderIso5218')
                            ? $responseJwt->claims()->get('genderIso5218')
                            : GenderIso5218Helper::NOT_SPECIFIED,
                    'status' => User::STATUS_ACTIVE,
                    'modifiedBy' => $userService->getById(1),
                ],
                $options
            );

            /** @var UserClientRoleService $userClientRoleService */
            $userClientRoleService = $this->applicationServicePluginManager->get(UserClientRoleService::class);
            /** @var RoleService $roleService */
            $roleService = $this->applicationServicePluginManager->get(RoleServiceInterface::class);

            foreach ($responseJwt->claims()->get('roleCollection') as $roleCode) {
                $role = $roleService->getOneBy(['code' => $roleCode]);

                if (!$userClientRoleService->getOneOrNullBy([
                    'userClient' => $userClientForClient,
                    'role' => $role,
                ])) {
                    $userClientRoleService->create(
                        [
                            'userClient' => $userClientForClient,
                            'role' => $role
                        ]
                    );
                }
            }

            foreach ($roleCodeDiff as $roleCode) {
                $role = $roleService->getOneBy(['code' => $roleCode]);
                $userClientRoleService->archive(
                    $userClientRoleService->getOneBy([
                        'userClient' => $userClientForClient,
                        'role' => $role,
                    ])
                );
            }
        }

        return $user;
    }

    public function buildJwt(
        TokenInterface $entity,
        UnencryptedToken $responseJwt
    ): UnencryptedToken {
        /** @var JwtService $jwtInfrastructureServiceAuthIssuer */
        $jwtInfrastructureServiceAuthIssuer = $this->infrastructureServicePluginManager->get(
            $this->config[AuthProviderServiceInterface::CONFIG_KEY_JWT_ISSUER_SERVICE_NAME]
        );
        $jwt = $jwtInfrastructureServiceAuthIssuer->build(
            $entity->getUuid(),
            $responseJwt->claims()->get(RegisteredClaims::ISSUED_AT),
            $responseJwt->claims()->get(RegisteredClaims::EXPIRATION_TIME),
            [
                'uuid' => $entity->getUuid(),
                'username' => $responseJwt->claims()->get('username'),
                'email' => $responseJwt->claims()->get('email'),
                'firstName' => $responseJwt->claims()->get('firstName'),
                'lastName' => $responseJwt->claims()->get('lastName'),
                'tokenType' => OAuth2Helper::TOKEN_TYPE_BEARER,
                'scope' => null,
                'roleCollection' => $responseJwt->claims()->get('roleCollection'),
            ]
        );

        return $jwt;
    }
}
