<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Application\Service;

use Smtm\Auth\Context\Client\Domain\ClientInterface;
use Smtm\Auth\Context\Token\Domain\TokenInterface;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface AuthProviderServiceInterface
{

    public const CONFIG_KEY_TOKEN_EXPIRATION_SECONDS = 'tokenExpirationSeconds';
    public const CONFIG_KEY_REFRESH_TOKEN_EXPIRATION_SECONDS = 'refreshTokenExpirationSeconds';
    public const CONFIG_KEY_JWT_ISSUER_SERVICE_NAME = 'jwtIssuerServiceName';
    public const CONFIG_KEY_JWT_VERIFIER_SERVICE_NAME = 'jwtVerifierServiceName';

    public static function getUuid(): string;

    public function createToken(
        ClientInterface $client,
        array $data,
        ?TokenInterface $entity = null,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): TokenInterface;

    public function refreshToken(
        ClientInterface $client,
        array $data,
        ?TokenInterface $entity = null,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): TokenInterface;
}
