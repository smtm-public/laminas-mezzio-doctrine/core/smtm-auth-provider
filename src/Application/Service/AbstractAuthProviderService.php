<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Application\Service;

use Smtm\Base\Application\Service\ApplicationServicePluginManagerAwareInterface;
use Smtm\Base\Application\Service\ApplicationServicePluginManagerAwareTrait;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractAuthProviderService implements
    AuthProviderServiceInterface,
    ApplicationServicePluginManagerAwareInterface,
    InfrastructureServicePluginManagerAwareInterface
{
    use ApplicationServicePluginManagerAwareTrait, InfrastructureServicePluginManagerAwareTrait;
}
