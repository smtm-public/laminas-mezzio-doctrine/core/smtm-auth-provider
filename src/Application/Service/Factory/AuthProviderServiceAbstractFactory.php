<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Application\Service\Factory;

use Smtm\AuthProvider\Application\Service\AbstractAuthProviderService;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthProviderServiceAbstractFactory implements AbstractFactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var AbstractAuthProviderService $service */
        $service = new $requestedName();
        $service->setApplicationServicePluginManager($container->get(ApplicationServicePluginManager::class));
        $service->setInfrastructureServicePluginManager($container->get(InfrastructureServicePluginManager::class));

        return $service;
    }

    public function canCreate(ContainerInterface $container, $requestedName)
    {
        return is_subclass_of($requestedName, AbstractAuthProviderService::class);
    }
}
