<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Application\Service\Factory;

use Smtm\AuthProvider\Application\Service\AuthProviderServicePluginManager;
use Smtm\AuthProvider\Application\Service\AuthProviderServicePluginManagerAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthProviderServicePluginManagerAwareDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var AuthProviderServicePluginManagerAwareInterface $object */
        $object = $callback();

        $object->setAuthProviderServicePluginManager($container->get(AuthProviderServicePluginManager::class));

        return $object;
    }
}
