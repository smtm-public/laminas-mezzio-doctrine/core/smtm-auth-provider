<?php

declare(strict_types=1);

namespace Smtm\AuthProvider;

use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigProvider
{
    #[ArrayShape([
        'auth-provider' => 'array',
        'dependencies' => 'array',
        'doctrine' => 'array',
        'routes' => 'array',
    ])] public function __invoke(): array
    {
        return [
            'auth-provider' => include __DIR__ . '/../config/auth-provider.php',
            'dependencies' => include __DIR__ . '/../config/dependencies.php',
            'doctrine' => include __DIR__ . '/../config/doctrine.php',
            'routes' => include __DIR__ . '/../config/routes.php',
        ];
    }
}
