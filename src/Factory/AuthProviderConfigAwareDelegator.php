<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Factory;

use Smtm\Base\Factory\ConfigAwareDelegator;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthProviderConfigAwareDelegator extends ConfigAwareDelegator
{
    protected array|string|null $configKey = 'auth-provider';
}
