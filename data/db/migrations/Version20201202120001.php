<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Migration;

use Smtm\Auth\Infrastructure\Doctrine\Migration\AuthRoleTableAndAuthPermissionTableAndAuthRoleCodePermissionTablePopulatingMigrationTrait;
use Smtm\Base\Infrastructure\Doctrine\Migration\TablePopulatingMigrationTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Psr\Log\LoggerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20201202120001 extends AbstractMigration
{

    use AuthRoleTableAndAuthPermissionTableAndAuthRoleCodePermissionTablePopulatingMigrationTrait,
        TablePopulatingMigrationTrait;

    public function __construct(Connection $connection, LoggerInterface $logger)
    {
        parent::__construct($connection, $logger);

        $this->permission = [
            'smtm.auth-provider.user.create' => [
                'uuid' => '5bae029d-d064-4c8b-8601-822048ec5809',
                'r_name' => 'smtm.auth-provider.user.create',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth-provider.user.read-by-token' => [
                'uuid' => '3c274130-98e1-4ed6-adb5-bd7155d66f30',
                'r_name' => 'smtm.auth-provider.user.read-by-token',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth-provider.user.read' => [
                'uuid' => 'e38e0101-b415-4392-864a-f0c774f723fb',
                'r_name' => 'smtm.auth-provider.user.read',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth-provider.user.index' => [
                'uuid' => 'b9530abd-3a40-4aff-9b1d-bdf279f3efd1',
                'r_name' => 'smtm.auth-provider.user.index',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth-provider.user.update' => [
                'uuid' => '0ff78342-5805-475e-93ed-c69c9ea48158',
                'r_name' => 'smtm.auth-provider.user.update',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
        ];
        $this->rolePermission = [
            'user' => [
                'smtm.auth-provider.user.read-by-token',
            ],
            'admin' => [
                'smtm.auth-provider.user.create',
                'smtm.auth-provider.user.read-by-token',
                'smtm.auth-provider.user.read',
                'smtm.auth-provider.user.index',
                'smtm.auth-provider.user.update',
            ],
        ];
    }

    public function up(Schema $schema): void
    {
        $this->populateAuthPermissionTable($this->connection, $schema);
        $this->populateAuthRoleCodePermissionTable($this->connection, $schema);
    }

    public function down(Schema $schema): void
    {

    }
}
