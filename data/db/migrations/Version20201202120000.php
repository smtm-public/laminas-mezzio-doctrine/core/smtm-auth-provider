<?php

declare(strict_types=1);

namespace Smtm\AuthProvider\Migration;

use Smtm\Base\Infrastructure\Doctrine\Migration\CommonMigrationTrait;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20201202120000 extends AbstractMigration
{

    use CommonMigrationTrait;

    public function up(Schema $schema): void
    {
        $this->createAuthProviderAuthCodeTable($schema);
        $this->createAuthProviderTokenTable($schema);
    }

    public function createAuthProviderAuthCodeTable(Schema $schema): void
    {
        $authProviderAuthCodeTable = $this->createTableAndPrimaryKey($schema, 'auth_provider_auth_code');
        $this->addUuidStringColumnAndUniqueIndex($authProviderAuthCodeTable);
        $authProviderAuthCodeTable->addColumn('auth_user_client_id', Types::INTEGER, ['notNull' => true]);
        $authProviderAuthCodeTable->addColumn('code', Types::TEXT, ['notNull' => true]);
        $authProviderAuthCodeTable->addColumn('redirect_uri', Types::STRING, ['length' => 255, 'notNull' => true]);
        $authProviderAuthCodeTable->addColumn('scope', Types::TEXT, ['notNull' => true]);
        $authProviderAuthCodeTable->addColumn('code_challenge', Types::STRING, ['length' => 128, 'notNull' => false]);
        $authProviderAuthCodeTable->addColumn('code_challenge_method', Types::STRING, ['length' => 32, 'notNull' => false]);
        $authProviderAuthCodeTable->addColumn('r_state', Types::STRING, ['length' => 255, 'notNull' => false]);
        $authProviderAuthCodeTable->addColumn('expires', Types::DATETIME_IMMUTABLE, ['notNull' => true]);
        $authProviderAuthCodeTable->addColumn('used', Types::SMALLINT, ['default' => 0, 'notNull' => true]);
        $authProviderAuthCodeTable->addIndex(['used'], 'idx_' . $authProviderAuthCodeTable->getName() . '_used');
        $authProviderAuthCodeTable->addColumn('revoked', Types::SMALLINT, ['default' => 0, 'notNull' => true]);
        $authProviderAuthCodeTable->addIndex(['revoked'], 'idx_' . $authProviderAuthCodeTable->getName() . '_revoked');
        $this->addCreatedDatetimeImmutableColumnAndIndex($authProviderAuthCodeTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($authProviderAuthCodeTable);
        $authProviderAuthCodeTable->addForeignKeyConstraint(
            $schema->getTable('auth_user_client'),
            ['auth_user_client_id'],
            ['id'],
            [],
            'fk_' . $authProviderAuthCodeTable->getName() . '_auth_user_client_id'
        );
        $authProviderAuthCodeTable->addIndex(['auth_user_client_id'], 'idx_' . $authProviderAuthCodeTable->getName() . '_auth_user_client_id');
        $authProviderAuthCodeTable->addIndex(
            ['auth_user_client_id', 'revoked'],
            'idx_' . $authProviderAuthCodeTable->getName() . '_auth_user_client_id_revoked'
        );
        $authProviderAuthCodeTable->addIndex(
            ['auth_user_client_id', 'used', 'revoked'],
            'idx_' . $authProviderAuthCodeTable->getName() . '_auth_user_client_id_used_revoked'
        );
    }

    public function createAuthProviderTokenTable(Schema $schema): void
    {
        $authProviderTokenTable = $this->createTableAndPrimaryKey($schema, 'auth_provider_token');
        $this->addUuidStringColumnAndUniqueIndex($authProviderTokenTable);
        $authProviderTokenTable->addColumn('provider_uuid', Types::STRING, ['length' => 36, 'notNull' => true]);
        $authProviderTokenTable->addColumn('auth_user_client_id', Types::INTEGER, ['notNull' => true]);
        $authProviderTokenTable->addForeignKeyConstraint(
            $schema->getTable('auth_user_client'),
            ['auth_user_client_id'],
            ['id'],
            [],
            'fk_' . $authProviderTokenTable->getName() . '_auth_user_client_id'
        );
        $authProviderTokenTable->addIndex(['auth_user_client_id'], 'idx_' . $authProviderTokenTable->getName() . '_auth_user_client_id');
        $authProviderTokenTable->addColumn('auth_consumer_token_id', Types::INTEGER, ['notNull' => false]);
        $authProviderTokenTable->addIndex(
            ['auth_consumer_token_id'],
            'idx_' . $authProviderTokenTable->getName() . '_auth_consumer_token_id'
        );
        $authProviderTokenTable->addForeignKeyConstraint(
            $schema->getTable('auth_consumer_token'),
            ['auth_consumer_token_id'],
            ['id'],
            [],
            'fk_' . $authProviderTokenTable->getName() . '_auth_consumer_token_id'
        );
        $authProviderTokenTable->addColumn('id_token', Types::TEXT, ['notNull' => false]);
        $authProviderTokenTable->addColumn('access_token', Types::TEXT, ['notNull' => true]);
        $authProviderTokenTable->addColumn('refresh_token', Types::TEXT, ['notNull' => false]);
        $authProviderTokenTable->addColumn('expires', Types::DATETIME_IMMUTABLE, ['notNull' => true]);
        $authProviderTokenTable->addColumn('refresh_token_expires', Types::DATETIME_IMMUTABLE, ['notNull' => false]);
        $authProviderTokenTable->addColumn('used', Types::SMALLINT, ['default' => 0, 'notNull' => true]);
        $authProviderTokenTable->addIndex(['used'], 'idx_' . $authProviderTokenTable->getName() . '_used');
        $authProviderTokenTable->addColumn('revoked', Types::SMALLINT, ['default' => 0, 'notNull' => true]);
        $authProviderTokenTable->addIndex(['revoked'], 'idx_' . $authProviderTokenTable->getName() . '_revoked');
        $authProviderTokenTable->addColumn('token_type', Types::STRING, ['length' => 32, 'notNull' => false]);
        $authProviderTokenTable->addColumn('r_scope', Types::STRING, ['length' => 255, 'notNull' => false]);
        $authProviderTokenTable->addIndex(
            ['auth_user_client_id', 'used', 'revoked'],
            'idx_' . $authProviderTokenTable->getName() . '_auth_user_client_id_used_revoked'
        );
        $this->addCreatedDatetimeImmutableColumnAndIndex($authProviderTokenTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($authProviderTokenTable);
        $this->addNotArchivedSmallintColumnAndIndex($authProviderTokenTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($authProviderTokenTable);
    }

    public function down(Schema $schema): void
    {

    }
}
