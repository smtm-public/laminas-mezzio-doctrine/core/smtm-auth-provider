### Deploy *Docker Stack*
1. (replace {SSH_KEY_NAME} with the file name of the private key)    
```
ssh-agent bash -c '
    cd /home/smtm;\
    ssh-add /root/.ssh/{SSH_KEY_NAME};\
    git clone git@gitlab.com:smtmbg/devops/docker-compose.git docker-compose\
'
```
2.    
```
cd docker-compose
```
3.    
```
cp .env.example .env
```
4. (replace {COMPOSE_PROJECT_NAME} with the name of the project)    
```
sed -i s/COMPOSE_PROJECT_NAME=default/COMPOSE_PROJECT_NAME={COMPOSE_PROJECT_NAME}/ .env
```
5.    
```
docker-compose build
```
6.    
```
docker-compose up -d
```
7. (replace {SSH_KEY_NAME} with the base name of the key pair)    
```
cp /root/.ssh/{SSH_KEY_NAME} /home/smtm/docker-compose/php8-fpm/volumes/directory/root/{SSH_KEY_NAME}
cp /root/.ssh/{SSH_KEY_NAME}.pub /home/smtm/docker-compose/php8-fpm/volumes/directory/root/{SSH_KEY_NAME}.pub
```
    
### Deploy *SSO Service*    
1. (replace {COMPOSE_PROJECT_NAME} with the name of the project, replace {SSH_KEY_NAME} with the file name of the private key)    
```
docker exec -it {COMPOSE_PROJECT_NAME}-php8-fpm\
    ssh-agent bash -c '\
        ssh-add /root/.ssh/{SSH_KEY_NAME};\
        composer create-project -n smtm/smtm-mezzio-skeleton /var/www/content/smtm-auth-provider --repository-url='"'"'{"url":"git@gitlab.com:smtmbg/apps/smtm-mezzio-skeleton.git","type":"git"}'"'"' --stability dev --keep-vcs\
    '
```
2. (replace {DB_USER} with the name of the database user, replace {DB_PASSWORD} with the password of the database user)    
```
sed -i s/DB_USER=user/DB_USER={DB_USER}/ volumes-common/directory/var/www/content/smtm-auth-provider/.env.doctrine
sed -i s/DB_PASSWORD=password/DB_PASSWORD={DB_PASSWORD}/ volumes-common/directory/var/www/content/smtm-auth-provider/.env.doctrine
sed -i s/DB_NAME=smtm/DB_NAME=smtm_auth_provider/ volumes-common/directory/var/www/content/smtm-auth-provider/.env.doctrine
```
3.
```
docker exec -it {COMPOSE_PROJECT_NAME}-mariadb \
    mysql -u root -p123123 -e 'CREATE DATABASE smtm_auth_provider;'
```
4. (replace {COMPOSE_PROJECT_NAME} with the name of the project)    
```
docker exec -it {COMPOSE_PROJECT_NAME}-php8-fpm\
    composer config -f /var/www/content/smtm-auth-provider/composer.json repositories.mezzio/mezzio-laminasviewrenderer git git@gitlab.com:smtmbg/apps/packages/mezzio/mezzio-laminasviewrenderer.git
```
5. (replace {COMPOSE_PROJECT_NAME} with the name of the project)    
```
docker exec -it {COMPOSE_PROJECT_NAME}-php8-fpm\
    composer config -f /var/www/content/smtm-auth-provider/composer.json --json --merge extra.laminas.component-whitelist '["smtm/mezzio-laminasviewrenderer"]
```
6.    
```
mv volumes-common/directory/var/www/content/smtm-auth-provider/composer.lock volumes-common/directory/var/www/content/smtm-auth-provider/composer.lock.bak
```
7. (replace {COMPOSE_PROJECT_NAME} with the name of the project, replace {SSH_KEY_NAME} with the file name of the private key)    
```
docker exec -it {COMPOSE_PROJECT_NAME}-php8-fpm\
    ssh-agent bash -c '\
        ssh-add /root/.ssh/{SSH_KEY_NAME};\
        cd /var/www/content/smtm-sso;\
        composer require smtm/smtm-sso:dev-master\
    '
```
8. Create nginx vhost config:        
```
TODO
```
9. (replace {COMPOSE_PROJECT_NAME} with the name of the project)    
```
docker exec -it {COMPOSE_PROJECT_NAME}-nginx\
    supervisorctl restart nginx
```
