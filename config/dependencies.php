<?php

declare(strict_types=1);

namespace Smtm\Sso;

use Smtm\Auth\Authentication\Application\Service\AuthenticationServicePluginManager;
use Smtm\AuthProvider\Authentication\Application\Service\Bearer\BearerAuthenticationService;
use Smtm\AuthProvider\Application\Service\AuthProviderServicePluginManager;
use Smtm\AuthProvider\Application\Service\SmtmAuthProviderService;
use Smtm\AuthProvider\Application\Service\Factory\AuthProviderServiceAbstractFactory;
use Smtm\AuthProvider\Application\Service\Factory\AuthProviderServicePluginManagerAwareDelegator;
use Smtm\AuthProvider\Application\Service\RemoteSmtmAuthProviderService;
use Smtm\AuthProvider\Context\AuthCode\Application\Service\AuthCodeService;
use Smtm\AuthProvider\Context\Login\Http\Handler\Factory\LoginHandlerFactory;
use Smtm\AuthProvider\Context\Login\Http\Handler\LoginHandler;
use Smtm\AuthProvider\Context\Login\Http\Handler\LoginHandlerInterface;
use Smtm\AuthProvider\Context\Token\Application\Service\TokenServiceInterface;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Doctrine\Orm\EntityManagerInterface;
use Smtm\Base\Infrastructure\Laminas\Validator\Factory\ValidatorPluginManagerAwareDelegator;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\AuthProvider\Context\Token\Application\Service\TokenService;
use Smtm\AuthProvider\Factory\AuthProviderConfigAwareDelegator;
use Psr\Container\ContainerInterface;

return [
    'factories' => [
        LoginHandler::class => function (ContainerInterface $container, $requestedName, array $options = null) {
            return $container->has(LoginHandlerInterface::class)
                ? $container->get(LoginHandlerInterface::class)
                : (new LoginHandlerFactory())->__invoke($container, $requestedName, $options);
        },

        AuthProviderServicePluginManager::class => function (ContainerInterface $container, $requestedName) {
            return new AuthProviderServicePluginManager($container);
        }
    ],

    'delegators' => [
        \Smtm\AuthProvider\Context\AuthCode\Http\Handler\AuthCodeHandler::class => [
            AuthProviderConfigAwareDelegator::class,
        ],
        \Smtm\AuthProvider\Context\Login\Http\Handler\LoginHandler::class => [
            AuthProviderConfigAwareDelegator::class,
        ],

        \Smtm\AuthProvider\Context\Test\Http\Handler\SessionHandler::class => [
            AuthProviderConfigAwareDelegator::class,
        ],
        \Smtm\AuthProvider\Context\Test\Http\Handler\LoginHandler::class => [
            AuthProviderConfigAwareDelegator::class,
        ],
        \Smtm\AuthProvider\Context\Test\Http\Handler\LogoutHandler::class => [
            AuthProviderConfigAwareDelegator::class,
        ],

        ApplicationServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var ApplicationServicePluginManager $applicationServicePluginManager */
                $applicationServicePluginManager = $callback();
                $applicationServicePluginManager->addDelegator(
                    AuthCodeService::class,
                    AuthProviderConfigAwareDelegator::class
                );
                $applicationServicePluginManager->addDelegator(
                    AuthCodeService::class,
                    ValidatorPluginManagerAwareDelegator::class
                );
                $applicationServicePluginManager->addDelegator(
                    TokenService::class,
                    AuthProviderConfigAwareDelegator::class
                );
                $applicationServicePluginManager->addDelegator(
                    TokenService::class,
                    AuthProviderServicePluginManagerAwareDelegator::class
                );
                $applicationServicePluginManager->setAlias(
                    TokenServiceInterface::class,
                    TokenService::class
                );

                return $applicationServicePluginManager;
            }
        ],

        AuthenticationServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var AuthenticationServicePluginManager $authenticationServicePluginManager */
                $authenticationServicePluginManager = $callback();

                $authenticationServicePluginManager->addDelegator(
                    BearerAuthenticationService::class,
                    AuthProviderConfigAwareDelegator::class
                );

                return $authenticationServicePluginManager;
            }
        ],

        AuthProviderServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var AuthProviderServicePluginManager $authProviderServicePluginManager */
                $authProviderServicePluginManager = $callback();

                $authProviderServicePluginManager->addAbstractFactory(
                    AuthProviderServiceAbstractFactory::class
                );

                $authProviderOptions = []; // TODO: MD - Maybe something values here from the config for the OptionsAwareInterface?
                /** @var ApplicationServicePluginManager $applicationServicePluginManager */
                $applicationServicePluginManager = $container->get(ApplicationServicePluginManager::class);
                /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
                $infrastructureServicePluginManager = $container->get(InfrastructureServicePluginManager::class);

                $authProviderServicePluginManager->addDelegator(
                    RemoteSmtmAuthProviderService::class,
                    AuthProviderConfigAwareDelegator::class
                );
                $authProviderServicePluginManager->setFactory(
                    SmtmAuthProviderService::class,
                    function (
                        ContainerInterface $container,
                        $name,
                        array $options = null
                    ) use (
                        $applicationServicePluginManager,
                        $infrastructureServicePluginManager,
                        $authProviderServicePluginManager,
                        $authProviderOptions
                    ) {
                        /** @var SmtmAuthProviderService $service */
                        $service = new $name($authProviderOptions);
                        $service->setApplicationServicePluginManager($applicationServicePluginManager);
                        $service->setInfrastructureServicePluginManager($infrastructureServicePluginManager);

                        return $service;
                    }
                );
                $authProviderServicePluginManager->addDelegator(
                    SmtmAuthProviderService::class,
                    AuthProviderConfigAwareDelegator::class
                );

                foreach ($container->get('config')['auth-provider']['providers'] as $authProviderConfig) {
                    $authProviderServicePluginManager->registerAuthProvider(
                        call_user_func([$authProviderConfig['class'], 'getUuid']),
                        $authProviderConfig['class']
                    );
                }

                return $authProviderServicePluginManager;
            }
        ],

        InfrastructureServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
                $infrastructureServicePluginManager = $callback();
                $infrastructureServicePluginManager->addDelegator(
                    EntityManagerInterface::class,
                    \Smtm\AuthProvider\Infrastructure\Doctrine\Orm\Factory\EnablePackageDoctineFiltersOnEntityMangerDelegator::class
                );
                $infrastructureServicePluginManager->addDelegator(
                    \Doctrine\ORM\EntityManagerInterface::class,
                    \Smtm\AuthProvider\Infrastructure\Doctrine\Orm\Factory\EnablePackageDoctineFiltersOnEntityMangerDelegator::class
                );

                return $infrastructureServicePluginManager;
            }
        ],
    ],
];
