<?php

declare(strict_types=1);

namespace Smtm\AuthProvider;

return [
    'em' => 'default',

    'table_storage' => [
        'table_name' => 'doctrine_migrations_smtm_auth_provider',
    ],

    'migrations_paths' => [
        'Smtm\AuthProvider\Migration' => __DIR__ . '/../data/db/migrations',
    ],
];
