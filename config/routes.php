<?php

declare(strict_types=1);

namespace Smtm\AuthProvider;

use Smtm\Auth\Authorization\Application\Service\AuthorizationService;
use Smtm\Base\Http\InputFilter\UuidRouteParamRequestValidatingInputFilterCollection;
use Smtm\Base\Infrastructure\Helper\EnvHelper;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Smtm\Auth\Context\User\Http\InputFilter\IndexHandlerRequestValidatingInputFilterCollection;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-auth-provider')) {
    $dotenv = \Dotenv\Dotenv::createMutable(__DIR__ . '/../../../../', '.env.smtm.smtm-auth-provider');
    $dotenv->load();
}

$exposedRoutes = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_PROVIDER_EXPOSED_ROUTES'),
    true,
    JSON_THROW_ON_ERROR
);
$routes = [
    'smtm.auth-provider.user.read-by-token' => [
        'path' => '/auth-provider/user-by-token',
        'method' => 'get',
        'middleware' => Context\User\Http\Handler\ReadByTokenHandler::class,
        'options' => [
            'authenticationServiceNames' => [
                HttpHelper::AUTHENTICATION_BEARER => BearerTokenAuthenticationService::class,
            ],
            'authorization' => [
                'options' => [
                    'required' => true,
                ],
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ],
            ],
            'requestValidatingInputFilterCollectionName' => null,
        ],
    ],

    'smtm.auth-provider.user.index' => [
        'path' => '/auth-provider/user',
        'method' => 'get',
        'middleware' => Context\User\Http\Handler\IndexHandler::class,
        'options' => [
            'ssoAuthentication' => [
                'options' => [
                    'required' => true,
                ],
                'services' => [
                    HttpHelper::AUTHENTICATION_TOKEN => [
                        'name' => SsoBearerJwtAuthenticationService::class,
                    ],
                ],
            ],
            'requestValidatingInputFilterCollectionName' =>
                IndexHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth-provider.user.create' => [
        'path' => '/auth-provider/user',
        'method' => 'post',
        'middleware' => Context\User\Http\Handler\CreateHandler::class,
        'options' => [
            'ssoAuthentication' => [
                'options' => [
                    'required' => true,
                ],
                'services' => [
                    HttpHelper::AUTHENTICATION_TOKEN => [
                        'name' => SsoBearerJwtAuthenticationService::class,
                    ],
                ],
            ],
            'requestValidatingInputFilterCollectionName' =>
                Context\User\Http\InputFilter\CreateHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth-provider.user.read' => [
        'path' => '/auth-provider/user/{uuid}',
        'method' => 'post',
        'middleware' => Context\User\Http\Handler\ReadHandler::class,
        'options' => [
            'ssoAuthentication' => [
                'options' => [
                    'required' => true,
                ],
                'services' => [
                    HttpHelper::AUTHENTICATION_TOKEN => [
                        'name' => SsoBearerJwtAuthenticationService::class,
                    ],
                ],
            ],
            'requestValidatingInputFilterCollectionName' =>
                UuidRouteParamRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth-provider.user.update' => [
        'path' => '/auth-provider/user/{uuid}',
        'method' => 'put',
        'middleware' => Context\User\Http\Handler\UpdateHandler::class,
        'options' => [
            'authentication' => [
                'options' => [
                    'required' => true,
                ],
                'services' => [
                    HttpHelper::AUTHENTICATION_TOKEN => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'requestValidatingInputFilterCollectionName' =>
                Context\User\Http\InputFilter\CreateHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth-provider.user.password.patch' => [
        'path' => '/auth-provider/user/password',
        'method' => 'patch',
        'middleware' => Context\User\Http\Handler\Password\PatchHandler::class,
        'options' => [
            'authentication' => null,
            'authorization' => null,
            'requestValidatingInputFilterCollectionName' =>
                Context\User\Http\InputFilter\Password\PatchHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth-provider.user.password.reset.initiate' => [
        'path' => '/auth-provider/user/password-reset/initiate',
        'method' => 'post',
        'middleware' => Context\User\Http\Handler\Password\ResetInitiateHandler::class,
        'options' => [
            'authentication' => null,
            'authorization' => null,
            'requestValidatingInputFilterCollectionName' =>
                Context\User\Http\InputFilter\Password\ResetInitiateHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth-provider.user.password.reset.complete' => [
        'path' => '/auth-provider/user/password-reset/complete',
        'method' => 'post',
        'middleware' => Context\User\Http\Handler\Password\ResetCompleteHandler::class,
        'options' => [
            'authentication' => null,
            'authorization' => null,
            'requestValidatingInputFilterCollectionName' =>
                Context\User\Http\InputFilter\Password\ResetCompleteHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth-provider.login.get' => [
        'path' => '/auth-provider/login',
        'method' => 'get',
        'middleware' => Context\Login\Http\Handler\LoginHandler::class,
        'options' => [
            'authentication' => null,
            'authorization' => null,
            'requestValidatingInputFilterCollectionName' =>
                Context\Login\Http\InputFilter\LoginHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth-provider.logout.get' => [
        'path' => '/auth-provider/logout',
        'method' => 'get',
        'middleware' => Context\Logout\Http\Handler\LogoutHandler::class,
        'options' => [
            'authentication' => null,
            'authorization' => null,
            'requestValidatingInputFilterCollectionName' => null,
        ],
    ],
    'smtm.auth-provider.login.post' => [
        'path' => '/auth-provider/login',
        'method' => 'post',
        'middleware' => Context\AuthCode\Http\Handler\AuthCodeHandler::class,
        'options' => [
            'authentication' => null,
            'authorization' => null,
            'requestValidatingInputFilterCollectionName' =>
                Context\AuthCode\Http\InputFilter\AuthCodeHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],

    'smtm.auth-provider.token.create' => [
        'path' => '/auth-provider/token',
        'method' => 'post',
        'middleware' => Context\Token\Http\Handler\CreateHandler::class,
        'options' => [
            'authentication' => null,
            'authorization' => null,
//            'requestValidatingInputFilterCollectionName' =>
//                Context\Jwt\Http\InputFilter\CreateHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],

    'smtm.auth-provider.test.session' => [
        'path' => '/auth-provider/test/session',
        'method' => 'get',
        'middleware' => Context\Test\Http\Handler\SessionHandler::class,
        'options' => [
            'authentication' => null,
            'authorization' => null,
            'requestValidatingInputFilterCollectionName' => null,
        ],
    ],
    'smtm.auth-provider.test.login' => [
        'path' => '/auth-provider/test/login',
        'method' => 'get',
        'middleware' => Context\Test\Http\Handler\LoginHandler::class,
        'options' => [
            'authentication' => null,
            'authorization' => null,
            'requestValidatingInputFilterCollectionName' => null,
        ],
    ],
    'smtm.auth-provider.test.logout' => [
        'path' => '/auth-provider/test/logout',
        'method' => 'get',
        'middleware' => Context\Test\Http\Handler\LogoutHandler::class,
        'options' => [
            'authentication' => null,
            'authorization' => null,
            'requestValidatingInputFilterCollectionName' => null,
        ],
    ],
];

return array_intersect_key(
    $routes,
    array_combine($exposedRoutes, $exposedRoutes)
);
