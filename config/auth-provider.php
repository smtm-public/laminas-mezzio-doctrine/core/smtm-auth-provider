<?php

declare(strict_types=1);

namespace Smtm\AuthProvider;

use Smtm\AuthProvider\Application\Service\AuthProviderServiceInterface;
use Smtm\Base\Infrastructure\Helper\EnvHelper;
use Laminas\Mime\Mime;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-auth-provider')) {
    $dotenv = \Dotenv\Dotenv::createMutable(__DIR__ . '/../../../../', '.env.smtm.smtm-auth-provider');
    $dotenv->load();
}

!defined('AUTH_PROVIDER_DEFAULT_AUTH_CODE_EXPIRATION_SECONDS') && define('AUTH_PROVIDER_DEFAULT_AUTH_CODE_EXPIRATION_SECONDS', 3600);
!defined('AUTH_PROVIDER_DEFAULT_TOKEN_EXPIRATION_SECONDS') && define('AUTH_PROVIDER_DEFAULT_TOKEN_EXPIRATION_SECONDS', 3600);
!defined('AUTH_PROVIDER_DEFAULT_REFRESH_TOKEN_EXPIRATION_SECONDS') && define('AUTH_PROVIDER_DEFAULT_REFRESH_TOKEN_EXPIRATION_SECONDS', 86400);

$authProviderOptions = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_PROVIDER_AUTH_PROVIDER_OPTIONS'),
    true,
    flags: JSON_THROW_ON_ERROR
);
$providers = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_PROVIDER_PROVIDERS'),
    true,
    flags: JSON_THROW_ON_ERROR
);

return [
    'providers' => $providers,

    AuthProviderServiceInterface::CONFIG_KEY_JWT_VERIFIER_SERVICE_NAME =>
        EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_PROVIDER_INFRASTRUCTURE_SERVICE_JWT_VERIFIER_SERVICE_NAME'),
    AuthProviderServiceInterface::CONFIG_KEY_JWT_ISSUER_SERVICE_NAME =>
        EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_PROVIDER_INFRASTRUCTURE_SERVICE_JWT_ISSUER_SERVICE_NAME'),
    'authCodeExpirationSeconds' => EnvHelper::getEnvFromProcessOrSuperGlobal(
        'SMTM_AUTH_PROVIDER_AUTH_CODE_EXPIRATION_SECONDS',
        AUTH_PROVIDER_DEFAULT_AUTH_CODE_EXPIRATION_SECONDS,
        EnvHelper::TYPE_INT
    ),
    AuthProviderServiceInterface::CONFIG_KEY_TOKEN_EXPIRATION_SECONDS => EnvHelper::getEnvFromProcessOrSuperGlobal(
        'SMTM_AUTH_PROVIDER_TOKEN_EXPIRATION_SECONDS',
        AUTH_PROVIDER_DEFAULT_TOKEN_EXPIRATION_SECONDS,
        EnvHelper::TYPE_INT
    ),
    AuthProviderServiceInterface::CONFIG_KEY_REFRESH_TOKEN_EXPIRATION_SECONDS => EnvHelper::getEnvFromProcessOrSuperGlobal(
        'SMTM_AUTH_PROVIDER_REFRESH_TOKEN_EXPIRATION_SECONDS',
        AUTH_PROVIDER_DEFAULT_REFRESH_TOKEN_EXPIRATION_SECONDS,
        EnvHelper::TYPE_INT
    ),
    'debug' => [
        'authCodeCreateThrowOnErrorNoRedirect' => filter_var(
            EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_PROVIDER_DEBUG_AUTH_CODE_CREATE_THROW_ON_ERROR_NO_REDIRECT'),
            FILTER_VALIDATE_BOOLEAN
        ),
    ],
    'test' => [
        'ssoBaseUrl' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_PROVIDER_TEST_REMOTE_SERVICE_BASE_URL'),
        'clientId' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_PROVIDER_TEST_CLIENT_ID'),
        'redirectUri' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_PROVIDER_TEST_REDIRECT_URI'),
        'scope' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_PROVIDER_TEST_SCOPE'),
    ],
];
