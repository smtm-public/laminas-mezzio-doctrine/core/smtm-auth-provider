<?php

declare(strict_types=1);

namespace Smtm\AuthProvider;

return [
    'orm' => [
        'mapping' => [
            'paths' => [
                __DIR__ . '/../src/Context/AuthCode/Infrastructure/Doctrine/Orm/Mapping',
                __DIR__ . '/../src/Context/Token/Infrastructure/Doctrine/Orm/Mapping',
            ],
        ],
        'query' => [
            'filters' => [
                \Smtm\AuthProvider\Context\AuthCode\Infrastructure\Doctrine\Orm\Query\Filter\AuthCodeFilter::NAME =>
                    \Smtm\AuthProvider\Context\AuthCode\Infrastructure\Doctrine\Orm\Query\Filter\AuthCodeFilter::class,
                \Smtm\AuthProvider\Context\AuthCode\Infrastructure\Doctrine\Orm\Query\Filter\ExcludeArchivedFilter::NAME =>
                    \Smtm\AuthProvider\Context\AuthCode\Infrastructure\Doctrine\Orm\Query\Filter\ExcludeArchivedFilter::class,
                \Smtm\AuthProvider\Context\Token\Infrastructure\Doctrine\Orm\Query\Filter\TokenFilter::NAME =>
                    \Smtm\AuthProvider\Context\Token\Infrastructure\Doctrine\Orm\Query\Filter\TokenFilter::class,
                \Smtm\AuthProvider\Context\Token\Infrastructure\Doctrine\Orm\Query\Filter\ExcludeArchivedFilter::NAME =>
                    \Smtm\AuthProvider\Context\Token\Infrastructure\Doctrine\Orm\Query\Filter\ExcludeArchivedFilter::class,
            ],
        ],
    ],
];
